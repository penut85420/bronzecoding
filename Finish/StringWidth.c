#include <stdio.h>
#include <string.h>

#define BYTE4 0b11110000
#define BYTE3 0b11100000
#define BYTE2 0b11000000

int charbyte(char c) {
	if ((c & BYTE4) == BYTE4) return 4;
	if ((c & BYTE3) == BYTE3) return 3;
	if ((c & BYTE2) == BYTE2) return 2;
	return 1;
}

int strwidth(char *_str) {
	int sum = 0;
	char *p = _str;
	while (*p) {
		int t = charbyte(*p);
		p += t;
		// for (int i = 0; i < t; i++, p++)
		// 	putchar(*p);
		// printf("\t%d\n", t);
		if (t >= 3) sum += 2;
		else sum += 1;
	}
	return sum;
}

void demo(char *s, int width, int left) {
	char *fs[] = {"[%*s]\n", "[%-*s]\n"};
	int len = strlen(s);
	int w = strwidth(s);
	int diff = len - w;
	printf(fs[left?1:0], width + diff, s);
}

int main() {
	char s[] = "ｦｧｯ";
	char t[] = "hiheyhello";

	printf("s\t%s\n", s);
	printf("Width\t%d\n", strwidth(s));
	printf("Length\t%ld\n", strlen(s));

	printf("\ns\t%s\n", t);
	printf("Width\t%d\n", strwidth(t));
	printf("Length\t%ld\n", strlen(t));

	printf("\nWidth control with printf\n");
	printf("[%-20s]\n[%-20s]\n", s, t);

	printf("\nWidth control demo\n");
	demo(s, 20, 1);
	demo(t, 20, 1);
	putchar('\n');
	return 0;
}