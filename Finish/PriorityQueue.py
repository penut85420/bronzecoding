import sys

class Node:
    def __init__(self, id, p):
        self.id = id
        self.p = p
    
    def __str__(self):
        return str([self.id, self.p])

class PriorityQueue:
    def __init__(self):
        self.tree = list()
    
    def add_node(self, node):
        # Insert the new node to the tail of list
        self.tree.append(node)
        i = len(self.tree) - 1
        parent_index = int(i / 2)

        # Begin heapify
        while self.tree[i].p < self.tree[parent_index].p:
            tmp = self.tree[i]
            self.tree[i] = self.tree[parent_index]
            self.tree[parent_index] = tmp
            i = parent_index
            parent_index = int(i / 2)

    def remove_node(self):
        # Remove the first node
        rtn_node = self.tree[0]
        last_node = self.tree[-1]
        self.tree.remove(last_node)
        if len(self.tree) > 0:
            # Move the last node to the root
            self.tree[0] = last_node
            i = 0
            # Compare to left and right child nodes
            lchild_idx = i * 2 + 1 # Left child index
            rchild_idx = i * 2 + 2 # Right child index
            try:
                while self.tree[i].p >= self.tree[lchild_idx].p or self.tree[i].p >= self.tree[rchild_idx].p:
                    if self.tree[i].p >= self.tree[lchild_idx].p:
                        tmp = self.tree[i]
                        self.tree[i] = self.tree[lchild_idx]
                        self.tree[lchild_idx] = tmp
                        i = lchild_idx
                        lchild_idx = i * 2 + 1
                        rchild_idx = i * 2 + 2
                    elif self.tree[i].p >= self.tree[rchild_idx].p:
                        tmp = self.tree[i]
                        self.tree[i] = self.tree[rchild_idx]
                        self.tree[rchild_idx] = tmp
                        i = rchild_idx
                        lchild_idx = i * 2 + 1
                        rchild_idx = i * 2 + 2
            except: 
                # If node index out of bound will receive a exception
                pass
        # print(self)
        return rtn_node
    
    def __str__(self):
        return str([str(t) for t in self.tree])

if __name__ == "__main__":
    # Example input
    # inn = input("Input: ")
    # inn = "5, 1, 7, 3, 8, 2, 4, 3, -1, 4"
    inn = "9, 2, 7, 2, 5, 2, 10, 1, -1, 3"
    # inn = "5, 10, 4, 20, 3, 1, 7, 50, -1, 2"
    
    # Splite input sequence into integer list
    inn = [int(i) for i in inn.split(', ')]

    # Insert nodes into priority queue
    pq = PriorityQueue()
    i = 0
    while inn[i] != -1:
        n = Node(inn[i], inn[i+1])
        pq.add_node(n)
        i += 2
    i += 1
    print("Next Pop %d Nodes" % inn[i])

    # Begin pop elements from priority queue
    # This will show the step of process
    result = list()
    print("State", pq)
    for i in range(0, inn[i]):
        result.append(pq.remove_node())
        print("Return", result[-1], '\n')
        print("State", pq)
    
    # Print the final output
    print(result[0].id, end='')
    for r in result[1:]:
        print(",", r.id, end='')
