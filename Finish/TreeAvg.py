class Tree:
    def __init__(self, content, left=None, right=None):
        self.content = content
        self.left = left
        self.right = right

def total(tree):
    if tree is None:
        return 0
    return total(tree.left) + total(tree.right) + tree.content

def total_num(tree):
    if tree is None:
        return 0
    return total_num(tree.left) + total_num(tree.right) + 1

def tree_avg(tree):
    return total(tree) / total_num(tree)

n1 = Tree(5)
n2 = Tree(7)
n3 = Tree(3)
n4 = Tree(4)
n5 = Tree(9)
n6 = Tree(3)
n7 = Tree(8)

# First level
n1.right = n3
n1.left = n2

# Second level
n2.left = n4
n2.right = n5

# Third level
n5.left = n6
n5.right = n7

print(total(n1))
print(total_num(n1))
print(tree_avg(n1))