#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int mainMenuCount = 3, gameMenuCount = 5, battleMenuCount = 6, monsterCount = 11;
char *mainMenuStr[] = {"[N] 開始遊戲", "[C] 繼續遊戲", "[E] 結束遊戲"};
char *gameMenuStr[] = {"[C] 挑戰關卡", "[V] 檢視主角", "[H] 住宿系統", "[S] 保存進度", "[E] 回到主選單"};
char *monsterName[] = {
    "[A] 史萊姆", "[B] 大野狼", "[C] 牛頭人",
    "[D] 半獸人", "[E] 殭屍", "[F] 石像鬼",
    "[G] 小惡魔", "[H] 巴風特", "[I] 烈焰飛龍", 
    "[J] 巨魔龍", "[K] 離開"
};
char *battleMenuStr[] = {
    "[A] 普通攻擊", "[H] 治癒術", "[S] 星爆氣流斬", 
    "[P] 蓄力", "[E] 蓄能", "[F] 逃跑"
};

struct PlayerData {
    char name[64];
    int exp, hp, mp, spd, atk, g;
    int mhp, mmp, lv;
    int is_charge;
    int skill;
};

struct MonsterData {
    char type[64];
    int hp, spd, atk, provide_exp, is_flee, provide_g;
};

void initialPlayer(struct PlayerData*);
void initialMonster(struct MonsterData*, int);
void printPlayerData(struct PlayerData);
void viewPlayerStatus(struct PlayerData);
void attackP2M(struct PlayerData*, struct MonsterData*);
void attackM2P(struct PlayerData*, struct MonsterData*);

void printMenu(char*[], int);
void initGame();
void game(struct PlayerData*);
char getcmd();
void clrbfr();
void battleChoose(struct PlayerData*);
void battle(char, struct PlayerData*);
void printBattleMenu(char*[], int, int);
void checkLevel(struct PlayerData*);
void rest(struct PlayerData*);
void save(struct PlayerData);
void load();

int main() {
    system("chcp 65001 & cls & clear");
    printMenu(mainMenuStr, mainMenuCount);
    for (char c = getcmd();; c = getcmd()) {
        switch (c) {
            case 'n':
                initGame();
                break;
            case 'c':
                load();
                break;
            case 'e':
                printf("\n下次再見啦！\n");
                return 0;
        }
        printMenu(mainMenuStr, mainMenuCount);
    }
    
    return 0;
}

void initialPlayer(struct PlayerData *p) {
    printf("\n請輸入玩家的名字：");
    fgets(p->name, sizeof(p->name), stdin);
    
    strtok(p->name, "\r\n");
    p->g = p->exp = 0;
    p->lv = 1;
    p->hp = p->mhp = rand() % 250 + 350;
    p->mp = p->mmp = rand() % 20 + 100;
    p->spd = rand() % 15 + 35;
    p->atk = rand() % 20 + 40;
    p->skill = 0b111001;
    p->is_charge = 0;
    putchar('\n');
    printf(
        "附近不知道為什麼長了一座中二之塔出來\n"
        "去打敗那座塔的主人ㄅ\n"
        "...\n"
        "你問我打贏他有什麼獎勵？\n"
        "隨便啦，我也不知道\n"
        "去就對惹 ㄏㄏㄏㄏㄏ\n\n"
    );
}

void initialMonster(struct MonsterData *m, int tier) {
    strcpy(m->type, monsterName[tier - 1] + 4);
    m->hp = rand() % 20 * tier + 150 * tier * tier;
    m->spd = rand() % 15 * tier  + 35 * tier * tier;
    m->atk = rand() % 10 * tier  + 30 * tier * tier;
    m->provide_exp = tier * 20 * tier;
    m->provide_g = tier * 15;
    m->is_flee = 0;
}

void printPlayerData(struct PlayerData p) {
    printf("[%s] 體力 %d/%d 魔力 %d/%d 攻擊力 %d 速度 %d\n",
        p.name, p.hp, p.mhp, p.mp, p.mmp, p.atk, p.spd);
}

void viewPlayerStatus(struct PlayerData p) {
    printf(
        "=================================\n"
        "[%s Lv.%d EXP %d/%d %d G]\n"
        "[HP %5d/%5d] [MP %5d/%5d]\n"
        "[ATK %4d] [SPD %4d]\n"
        "=================================\n\n",
        p.name, p.lv, p.exp, p.lv * 100, p.g,
        p.hp, p.mhp, p.mp, p.mmp,
        p.atk, p.spd
    );

}

void printMonsterData(struct MonsterData m) {
    printf("[%s] 體力 %d 攻擊力 %d 速度 %d\n",
        m.type, m.hp, m.atk, m.spd);
}

void attackP2M(struct PlayerData *p, struct MonsterData *m) {
    char cmd;
    printPlayerData(*p);
    printMonsterData(*m);
    printBattleMenu(battleMenuStr, battleMenuCount, p->skill);
    for (char c = getcmd();; c = getcmd()) {
        int diff = p->spd - m->spd;
        int is_flee = rand() % 100 < 50 + diff;
        switch (c) {
        case 'a':
            if (p->is_charge) {
                p->is_charge = 0;
                m->hp -= p->atk * 25 / 10;
                printf("%s 發動蓄力攻擊！造成 %s 失血 %d 點。\n", p->name, m->type, p->atk * 25 / 10);
            } else {
                m->hp -= p->atk;
                printf("%s 攻擊，造成 %s 失血 %d 點。\n", p->name, m->type, p->atk);
            }
            
            return ;
        case 'h':
            if (!(p->skill & 2)) break;
            if (p->mp < p->mmp / 5) {
                printf("MP 不足！\n");
                break;
            }

            p->mp -= p->mmp / 5;
            p->hp += p->mhp / 5;
            if (p->hp > p->mhp) p->hp = p->mhp;
            printf("%s 使用治癒術，回復血量 %d 點！\n", p->name, 100);
            printPlayerData(*p);
            return ;

        case 's':
            if (!(p->skill & 4)) break;
            if (p->mp < p->mmp / 2) {
                printf("MP 不足！\n");
                break;
            }

            p->mp -= p->mmp / 2;
            m->hp -= p->atk * 5;
            printf(
                "%s 拔出雙刀，右劍橫斬，左劍立刻刺入，然後向右上帶出\n"
                "跳起同時轉身，順勢雙劍再向右砍一刀，再轉一圈，於 %s 胸腹向右一口氣畫一個等號\n"
                "著地再彈起同時拿起劍，然後在 %s 身上向下畫一個完美的 X 字\n"
                "順勢以完全相反的方向再畫一個完美的 X 字\n"
                "%s 慘叫一聲，%s 右劍下斬，但被 %s 打斷\n"
                "%s 雙劍齊插，左劍向左下斬同時右劍向右上斬\n"
                "雙劍再插，但被 %s 打斷，%s 嘶吼：（不要再插了，我受不住）\n"
                "%s 反擊，但被 %s 格檔，順勢反手拿劍，雙劍向內斬，同時換回正手拿劍\n"
                "再向外斬，然後向內斬儲氣，吃 %s 一拳並破壞儲氣\n"
                "%s 開始加速，左劍拉後儲氣，右劍向右砍再向左斬\n"
                "右劍再向右斬，左劍開始上前，左劍上斬，右劍再右斬\n"
                "左劍拿下至頭上高一些，右劍再下斬\n"
                "左劍向右擺右劍同時向右斬，此時 %s 加速完畢\n"
                "左劍拉下，右劍突刺，失敗並被 %s 捉住\n"
                "左劍向 %s 膝蓋一刺\n"
                ".\n.\n.\n對 %s 造成了 %d 點巨量的傷害\n",
                p->name, m->type, m->type, m->type, p->name, m->type, p->name,
                m->type, m->type, m->type, p->name, m->type, p->name, p->name,
                m->type, m->type, m->type, p->atk * 5
            ); return ;
        case 'p':
            p->is_charge = 1;
            printf("%s 發動了蓄力！\n", p->name);
            return ;
        case 'e':
            p->mp += p->mmp * 15 / 100;
            if (p->mp > p->mmp) p->mp = p->mmp;
            printf("%s 發動了蓄能，回復 MP %d 點！\n", p->name, p->mmp * 15 / 100);
            return ;
        case 'f':
            if (is_flee) {
                printf("逃跑成功！\n");
                m->is_flee = 1;
            } else printf("逃跑失敗﹍\n");
            return ;
        }
        printf("\n");
        printMenu(battleMenuStr, battleMenuCount);
    }
    
}

void attackM2P(struct PlayerData *p, struct MonsterData *m) {
    p->hp -= m->atk;
    printf("%s 攻擊，造成 %s 失血 %d 點。\n\n", m->type, p->name, m->atk);
}

void printMenu(char *cmd_str[], int n) {
    for (int i = 0; i < n; i++)
        printf("%s\n", cmd_str[i]);
    printf(" > ");
}

void clrbfr() {
    while (getchar() != '\n');
}

char getcmd() {
    char c = getchar();
    while (c == '\n') c = getchar();
    clrbfr();
    return c | 32;
}

void initGame() {
    struct PlayerData p;
    initialPlayer(&p);
    game(&p);
}

void game(struct PlayerData *p) {
    printMenu(gameMenuStr, gameMenuCount);
    for (char c = getcmd();; c = getcmd()) {
        switch (c) {
            case 'c':
                battleChoose(p);
                break;
            case 'v':
                viewPlayerStatus(*p);
                break;
            case 'h':
                rest(p);
                break;
            case 's':
                save(*p);
                break;
            case 'e':
                printf("\n大俠保重！\n\n");
                return ;
        }
        if (p->hp <= 0) {
            printf("\nGame Over...\n\n");
            return ;
        }
        printMenu(gameMenuStr, gameMenuCount);
    }
}

void battleChoose(struct PlayerData *p) {
    printMenu(monsterName, monsterCount);
    for (char c = getcmd();; c = getcmd()) {
        if (c == 'k') return ;
        else if (c >= 'a' && c <= 'j') {
            battle(c, p);
            return ;
        }
        printMenu(monsterName, monsterCount);
    }
}

void battle(char tier, struct PlayerData *p) {
    struct MonsterData m;

    initialMonster(&m, tier - 'a' + 1);

    printf("\n遭遇 %s！\n", m.type);

    printPlayerData(*p);
    printMonsterData(m);
    putchar('\n');
    if (m.spd > p->spd) attackM2P(p, &m);
    while (m.hp > 0 && p->hp > 0) {
        attackP2M(p, &m);
        if (m.is_flee) break;
        if (m.hp > 0) attackM2P(p, &m);
    }

    if (m.hp > 0 && !m.is_flee)
        printf("%s 死了QQ\n", p->name);
    else if (m.hp <= 0) {
        printf("\\%s 被打敗惹/\n"
            "%s 獲得金錢 %d G\n"
            "%s 獲得經驗值 %d!!\n\n", 
            m.type, p->name, m.provide_g, p->name, m.provide_exp);
        p->exp += m.provide_exp;
        p->g += m.provide_g;
        checkLevel(p);
    }
}

void printBattleMenu(char *str[], int count, int skill) {
    for (int i = 1, j = 0; j < count; i <<= 1, j++) {
        // printf("%d %d %s", i, j, str[j]);
        if (i & skill) printf("%s\n", str[j]);
    }
    printf(" > ");
}

void checkLevel(struct PlayerData *p) {
    if (p->exp >= p->lv * 100) {
        printf(
            "%s升級了！等級提昇至 %d\n"
            "最大 HP 上升 %d\n"
            "最大 MP 上升 %d\n"
            "攻擊力提昇 %d\n"
            "速度提昇 %d\n"
            "HP, MP 全部恢復！\n",
            p->name, p->lv + 1,
            p->mhp / 10, p->mmp / 10,
            p->atk / 5, p->spd * 15 / 100
        );
        p->lv++;
        p->mhp += p->mhp / 10;
        p->mmp += p->mmp / 10;
        p->atk += p->atk / 5;
        p->spd += p->spd * 15 / 100;
        p->hp = p->mhp;
        p->mp = p->mmp;
        p->exp = 0;

        if (p->lv >= 2 && !(p->skill & 2)) {
            p->skill |= 2;
            printf("%s 學會了治癒術！\n", p->name);
        }

        if (p->lv >= 3 && !(p->skill & 4)) {
            p->skill |= 4;
            printf("%s 學會了星爆氣流斬！\n", p->name);
        }

        printf("\n");
    }
}

void rest(struct PlayerData *p) {
    if (p->g < p->lv * 10) {
        printf("金幣不足，需要 %d，你只有 %d\n\n", p->lv * 10, p->g);
        return ;
    }

    printf(
        "住宿一晚需要 %d，確定要住嗎？\n"
        "[Y] 好啊\n[N] 好貴喔，不要\n", p->lv * 10
    );

    for (char c = getcmd();; c = getcmd()) {
        switch (c) {
            case 'y':
                p->g -= p->lv * 10;
                p->hp = p->mhp;
                p->mp = p->mmp;
                printf("\n睡了一覺，HP 與 MP 完全回復！\n\n");
                return ;
            case 'n':
                return ;
            default:
                printf("到底要還是不要？\n > ");
        }
    }
}


int isValidStr(char s[]) {
    for (int i = 0; s[i]; i++) {
        if (s[i] == '_') ;
        else if (s[i] >= 'a' && s[i] <= 'z') ;
        else if (s[i] >= 'A' && s[i] <= 'Z') ;
        else if (s[i] >= '0' && s[i] <= '9') ;
        else return 0;
    }
    return 1;
}

void save(struct PlayerData p) {
    char file[512];
    printf("請輸入存檔編號（只能包含英數字、底線）\n > ");
    scanf("%s", file);
    while (!isValidStr(file)) {
        printf("請輸入存檔編號（只能包含英數字、底線）\n > ");
        scanf("%s", file);
    }
    FILE *tmp = fopen(file, "rb");
    if (tmp != NULL) {
        printf("存檔已存在，是否覆蓋？(Y/N) ");
        char c = getcmd();
        if (c != 'y') {
            printf("存檔取消\n\n");
            return ;
        }
    }
    FILE *f = fopen(file, "wb");
    printf("存檔中...\n");
    fwrite(&p, sizeof(struct PlayerData), 1, f);
    printf("存檔成功！\n\n");
    fclose(f);
}

void load() {
    char file[512];
    printf("請輸入存檔編號（只能包含英數字、底線）\n > ");
    scanf("%s", file);
    while (!isValidStr(file)) {
        printf("請輸入存檔編號（只能包含英數字、底線）\n > ");
        scanf("%s", file);
    }
    struct PlayerData p;
    FILE *f = fopen(file, "rb");
    if (f == NULL) {
        printf("存檔不存在！\n");
        return ;
    }
    printf("讀取中...\n");
    fread(&p, sizeof(struct PlayerData), 1, f);
    printf("讀檔成功！\n\n");
    fclose(f);
    game(&p);
}