#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

void* foo(void* n) {
	char cmd[32];
	int nn = *(int*)n;
	sprintf(cmd, "sleep 0.%02d", nn);
	system(cmd);
	printf("%d ", nn);
	pthread_exit(NULL);
}

int main() {
	int arr[32], i = 0;

	while (scanf("%d", arr + i) != EOF) i++;

	pthread_t t[32];
	for (int j = 0; j < i; j++)
		pthread_create(t + j, NULL, foo, (void*)&arr[j]);

	for (int j = 0; j < i; j ++)
		pthread_join(t[j], NULL);
	putchar('\n');
	
	return 0;
}