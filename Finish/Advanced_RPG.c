#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define MONSTER_TOTAL 30

struct PlayerData {
    char name[64];
    int exp, hp, mp, spd, atk;
};

struct MonsterData {
    char type[64];
    int hp, spd, atk, provide_exp, is_flee;
};

void initialPlayer(struct PlayerData*);
void initialMonster(struct MonsterData*);
void printPlayerData(struct PlayerData);

void attackP2M(struct PlayerData*, struct MonsterData*);
void attackM2P(struct PlayerData*, struct MonsterData*);

void printCommand();

int main() {
    struct PlayerData p;
    struct MonsterData m[30];

    initialPlayer(&p);
    for (int i = 0; i < MONSTER_TOTAL; i++)
        initialMonster(&m[i]);

    for (int i = 0; i < MONSTER_TOTAL; i++) {
        // printPlayerData(p);
        printf("\n遭遇%s！\n", m[i].type);
        if (m[i].spd > p.spd) attackM2P(&p, &m[i]);
        while (m[i].hp > 0 && p.hp > 0) {
            attackP2M(&p, &m[i]);
            if (m[i].is_flee) break;
            if (m[i].hp > 0) attackM2P(&p, &m[i]);

        }

        if (m[i].hp > 0 && !m[i].is_flee) {
            printf("%s死了QQ\n", p.name);
            break;
        } else if (m[i].hp <= 0) {
            printf("\\%s被打敗惹/\n"
                "%s獲得經驗值 %d!!\n\n", 
                m[i].type, p.name, m[i].provide_exp);
            p.exp += m[i].provide_exp;
        }
    }

    return 0;
}

void initialPlayer(struct PlayerData *p) {
    printf("請輸入玩家的名字：");
    fgets(p->name, sizeof(p->name), stdin);
    
    strtok(p->name, "\r\n");
    p->exp = 0;
    p->hp = rand() % 250 + 350;
    p->mp = rand() % 20 + 100;
    p->spd = rand() % 15 + 35;
    p->atk = rand() % 20 + 40;
    
}

void initialMonster(struct MonsterData *m) {
    strcpy(m->type, "半獸人");
    m->hp = rand() % 20 + 150;
    m->spd = rand() % 15 + 35;
    m->atk = rand() % 10 + 30;
    m->provide_exp = 15;
    m->is_flee = 0;
}

void printPlayerData(struct PlayerData p) {
    printf("\n[%s] 體力%4d 魔力%4d 速度%3d 攻擊力 %3d 經驗值 %5d\n",
        p.name, p.hp, p.mp, p.spd, p.atk, p.exp);
}

void attackP2M(struct PlayerData *p, struct MonsterData *m) {
    char cmd;
    printPlayerData(*p);
    printCommand();
    cmd = getchar();
    if (cmd == '\n') cmd = getchar();
    cmd |= 32;
    while (1) {
        if (cmd == 'a') {
            m->hp -= p->atk;
            printf("%s攻擊，造成%s失血 %d 點。\n", p->name, m->type, p->atk);
            return ;
        }

        if (cmd == 'h') {
            if (p->mp >= 20) {
                p->mp -= 20;
                p->hp += 100;
                printf("%s使用治癒術，回復血量 %d 點！\n", p->name, 100);
                printPlayerData(*p);
                return ;
            } else {
                printf("MP 不足！\n");
            }
        }

        if (cmd == 'f') {
            int diff = p->spd - m->spd;
            int is_flee = rand() % 100 < 50 + diff;
            if (is_flee) {
                printf("逃跑成功！\n");
                m->is_flee = 1;
            } else printf("逃跑失敗﹍\n");
            return ;
        }
        printf("\n");
        printCommand();
        cmd = getchar();
        if (cmd == '\n') cmd = getchar();
        cmd |= 32;
    }
    
}

void attackM2P(struct PlayerData *p, struct MonsterData *m) {
    p->hp -= m->atk;
    printf("%s攻擊，造成%s失血 %d 點。\n", m->type, p->name, m->atk);
    printf("\a");
}

void printCommand() {
    char *cmd_str[] = {"[A] Attack", "[H] Heal", "[F] Flee"};
    int cmd_count = 3;
    for (int i = 0; i < 3; i++) 
        printf("%s\n", cmd_str[i]);
    printf(" > ");
}