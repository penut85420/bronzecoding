#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TYPE "test"
#define LEXICON_PATH "./lexicon_"TYPE".txt"
#define MAX_WORDS 100

#define INPUT_PATH "./wsegInput_"TYPE".txt"
#define OUTPUT_PATH "./wsegOutput_"TYPE".xml"

typedef struct {
	char **wordsList;
	int maxLen;
	int total;
} Lexicon;

Lexicon lexicon;

void loadLexicon();
void printLexicon();
void wseg(char*, char*);
void beginWseg(char*);
int match(char*);
int isInlist(char*);

int main() {
	loadLexicon();
	// printLexicon();
	char line[512];
	freopen(INPUT_PATH, "r", stdin);
	freopen(OUTPUT_PATH, "w", stdout);
	printf("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n");
	printf("<TOPICSET>\n");
	while (fgets(line, sizeof(line), stdin)) {
		strtok(line, "\r\n");
		printf("\t<TOPIC>\n");
		printf("\t\t<SENTENCE>%s</SENTENCE>\n", line);
		printf("\t\t<SEGMENTATION>\n");
		beginWseg(line);
		printf("\t\t</SEGMENTATION>\n");
		printf("\t</TOPIC>\n");
	} printf("</TOPICSET>\n");
	free(lexicon.wordsList);
	return 0;
}

void loadLexicon() {
	FILE *fp = fopen(LEXICON_PATH, "r");

	lexicon.wordsList = (char**) malloc (sizeof(char*) * MAX_WORDS);
	lexicon.maxLen = 0;

	char tmp[64];

	for (lexicon.total = 0; fscanf(fp, "%s", tmp) != EOF; lexicon.total++) {
		int len = strlen(tmp), i = lexicon.total;
		lexicon.wordsList[i] = (char*) malloc (sizeof(char) * len + 1);
		strcpy(lexicon.wordsList[i], tmp);
		if (len > lexicon.maxLen) lexicon.maxLen = len;
	}
}

void printLexicon() {
	for (int i = 0; i < lexicon.total; i++)
		printf("[%s], ", lexicon.wordsList[i]);
	printf("%d, %d\n", lexicon.total, lexicon.maxLen);
}

int match(char *s) {
	int len = strlen(s);
	len = len > lexicon.maxLen? lexicon.maxLen: len;
	for (int i = len; i > 0; i--) {
		char tmp[64];
		strncpy(tmp, s, i);
		tmp[i] = 0;
		if (isInlist(tmp)) return i;
	}
	return 0;
}

int isInlist(char *s) {
	for (int i = 0; i < lexicon.total; i++)
		if (!strcmp(lexicon.wordsList[i], s)) return 1;
	return 0;
}

void beginWseg(char *s) {
	char *result = (char*) malloc (sizeof(char) * strlen(s) * 2);
	wseg(s, result);
	free(result);
}

void wseg(char *s, char *result) {
	if (strlen(s) == 0) {
		printf("\t\t\t<SEGSEQ>%s</SEGSEQ>\n", result + 1);
		return;
	}
	int len = match(s);
	for (int i = len; i >= 1; i--) {
		char tmp[i + 1];
		strncpy(tmp, s, i);
		tmp[i] = 0;
		if (isInlist(tmp)) {
			char t_result[strlen(result) + strlen(tmp) + 2];
			sprintf(t_result, "%s %s", result, tmp);
			char *p = s + strlen(tmp);
			wseg(p, t_result);
		}
	}
}