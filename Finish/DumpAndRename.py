import os
import re
import subprocess

# for dirPath, _, fileList in os.walk('./'):
# 	for fileName in fileList:
# 		if '.c' in fileName:
# 			filePath = os.path.join(dirPath, fileName)
# 			newName = filePath.replace('/', '_')
# 			subprocess.call(["cp", filePath, os.path.join('./Dump/', newName[1:])])

# r = re.compile("([0-9]*\\-[0-9]*)")
# rep = dict()
# for dirPath, _, fileList in os.walk('./Dump'):
# 	for fileName in fileList:
# 		if 'Receive' in fileName: continue
# 		if 'Temp' in fileName: continue
# 		m = r.search(fileName)
# 		if m:
# 			a, b = m.group(1).split('-')
# 			a = int(a)
# 			b = int(b)
# 			newName = '%02d-%d' % (a, b)
# 			if 'Homework' in fileName: newName = 'hw' + newName
# 			elif 'Practice' in fileName: newName = 'lab' + newName
# 			if rep.get(newName, None): print("Rep", newName)
# 			else:
# 				rep[newName] = 1
# 				os.rename(os.path.join(dirPath, fileName), os.path.join(dirPath, newName))

for dirPath, _, fileList in os.walk('./Dump'):
	for fileName in fileList:
		os.rename(os.path.join(dirPath, fileName), os.path.join(dirPath, fileName) + '.c')