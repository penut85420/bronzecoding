#include <stdio.h>
#define SIZE 8

int board[SIZE][SIZE];
int player, bw_count[3];
char symbol_list[] = {' ', 'X', 'O'};
char *symbol = symbol_list + 1;
int direction[8][2] = {
	{ 1,  1},
	{ 1,  0},
	{ 1, -1},
	{ 0,  1},
	{ 0, -1},
	{-1,  1},
	{-1,  0},
	{-1, -1}
};

void initBoard();
void printBoard();
int hasFlipLine(int, int, int);
int isInBound(int, int);

int main() {
	initBoard();
	printBoard();
	printf("%d\n", hasFlipLine(1, 0, 0));
	printf("%d\n", hasFlipLine(1, 4, 4));
	printf("%d\n", hasFlipLine(1, 0, 0));
	printf("%d\n", hasFlipLine(1, 0, 0));
	printf("%d\n", hasFlipLine(1, 0, 0));
	return 0;
}

void initBoard() {
	for (int i = 0; i < SIZE; i++)
	for (int j = 0; j < SIZE; j++)
		board[i][j] = -1;
	board[SIZE/2][SIZE/2] = 1;
	board[SIZE/2-1][SIZE/2-1] = 1;
	board[SIZE/2][SIZE/2-1] = 0;
	board[SIZE/2-1][SIZE/2] = 0;
}

void printBoard() {
	bw_count[0] = bw_count[1] = 0;
	for (int i = 0 & printf("  "); i < SIZE; putchar('0' + i++)); putchar('\n');
	for (int i = 0; i < SIZE && putchar('0' + i) && putchar('|'); i++, printf("|\n"))
	for (int j = 0; j < SIZE; j++) {
		putchar(symbol[board[i][j]]);
		bw_count[(board[i][j] + 3) % 3]++;
	} printf("Black: %d, White: %d\n", bw_count[0], bw_count[1]);
}

int hasFlipLine(int player, int x, int y) {
	if (board[x][y] != -1) return 0;

	for (int i = 0; i < 8; i++)

	for (int i = 0; i < 8; i++) {
		int nx = x + direction[i][0], ny = y + direction[i][1];
		while (isInBound(nx, ny)) {
			if (board[nx][ny] == player) return 1;
			nx += direction[i][0];
			ny += direction[i][1];
		}
	}

	return 0;
}

int isInBound(int x, int y) {
	if (x < 0) return 0;
	if (y < 0) return 0;
	if (x >= SIZE) return 0;
	if (y >= SIZE) return 0;
	return 1;
}