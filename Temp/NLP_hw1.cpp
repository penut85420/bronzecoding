#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
using namespace std;

void div_lex( string sentence, string sen_part);
void deleteAllMark(string &s, const string &mark);
vector<string> dictionary;
vector<string> segL;

int main(void)
{
    system("chcp 65001");
	FILE *fp  = fopen("wsegInput_sample.txt", "r, ccs=UTF-8");
	FILE *fp2 = fopen("lexicon_sample.txt", "r, ccs=UTF-8");
	FILE *out = fopen("output.txt", "w, ccs=UTF-8");

	string dic_lex  = "";
	string sentence = "";
	string sen_part = "";
	char input[20];


	if (!fp) {
		printf("open input file fail\n");
		return 1;
	}else {
		printf("input opening\n");
	}

	if (!fp2) {
		printf("open dictionary fail\n");
		return 1;
	}else {
		printf("dictionary opening\n");
	}
	printf("-----\n");

	char tmp[64];
    while (fscanf(fp, "%s", tmp) != EOF) {
		sentence = string(tmp);
		cout << sentence << endl;
	}
	fclose(fp);

	printf("++++\n");

	while (fscanf(fp2, "%s", input) != EOF) {
        printf("%s\n", input);
		//dic_lex = input;  //trans to string
		//dictionary.push_back(dic_lex);
	}
	fclose(fp2);

	fprintf(out, "< ? xml version = ""1.0"" encoding = ""utf - 8"" ? >\n<TOPICSET>\n");
	fclose(out);
	/*
	while (getline(file, sentence)) {
		//cout << buffer2 << endl;
		file_O << " <TOPIC>\n  <SENTENCE>" << sentence << "</SENTENCE>\n";
		file_O << "  <SEGMENTATION>\n";
		div_lex( sentence, sen_part);
		cout << "len = " << segL.size() << endl;
		for (int n = 0; n < segL.size(); n++) {
			//cout << "in" << endl;
			cout << segL[n] << endl;
			file_O << "   <SEGSEQ>" << segL[n] << "</SEGSEQ>\n";
		}
		file_O << "  </SEGMENTATION>\n </TOPIC>\n";
	}
	file.close();

	cout << "write finish";
	file_O.close();
	*/
	return 0;
}

void deleteAllMark(string &s, const string &mark)
{
	size_t nSize = mark.size();
	while (1)
	{
		size_t pos = s.find(mark);
		if (pos == string::npos)
		{
			return;
		}

		s.erase(pos, nSize);
	}
}

void div_lex( string sentence, string sen_part) {
	int len = sentence.size();
	if (sentence.empty()) {
		deleteAllMark(sen_part, " ");
		segL.push_back(sen_part);
		return;
	}
	cout << "matching sentence : " + sentence << endl;
	for (int i = 1; i <= len; i++) {
		string word = sentence.substr(0, i);
		//cout << word << endl;
		for (int j=0; j < dictionary.size(); j++) {
			if (dictionary[j] == word) {
				div_lex(sentence.substr(i), sen_part + word + " ");
				cout << sen_part << endl;
				break;
			}
		}
	}
	return;
}