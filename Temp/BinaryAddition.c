/* PRESET CODE BEGIN - NEVER TOUCH CODE BELOW */

//#define _CRT_SECURE_NO_WARNINGS
#define MAXSIZE 128
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void binaryAddition(char string1[], char string2[]);

int main()
{
    char string1[MAXSIZE], string2[MAXSIZE];

    scanf("%s", string1);
    scanf("%s", string2);
    binaryAddition(string1, string2);

    //system("PAUSE");
    return 0;
}

/* PRESET CODE END - NEVER TOUCH CODE ABOVE*/

void binaryAddition(char string1[], char string2[])
{
    long long int i, a, b;

    for (a = i = 0; string1[i]; i++)
        a = a * 2 + string1[i] - '0';
    for (b = i = 0; string2[i]; i++)
        b = b * 2 + string2[i] - '0';

    a += b;

    for (i = 1; i <= a; i *= 2)
        ;
    i /= 2;
    while (i >= 1)
    {
        printf("%lld", a / i);
        a %= i;
        i /= 2;
    }
    printf("\n");
}