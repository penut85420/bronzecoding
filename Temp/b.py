import math
import random

R = 2        # Radius of the circle
P = 0        # Count points inside circle
N = 10000000 # Place N point

for i in range(N):
    # Random generate a coordinate inside square
    # -R < rx, ry < R
    rx = random.random() * 2 * R - R
    ry = random.random() * 2 * R - R

    # Follow the equation of a circle: x^2 + y^2 = R^2
    if rx * rx + ry * ry <= R * R:
        P += 1

# Calculate the percentage of points that inside a circle
print("Monte Carlo Method:")
print(P / N * (2 * R) ** 2)
print(4 * P / N)
print("πr^2:")
print(R * R * math.pi)