---
tags: C Programming
---
# C Programming Final Simulation Examination

## A. Write down the output
1.
    ```c=
    #include <stdio.h>
    #include <string.h>

    int main() {
        char s1[] = "You do not need to modified your program. "
            "The BasicPractice webpage version is exactly "
            "the same as the one on OnlineJudge or E-tutor.";
        char trouble[512] = { 0 };
        char token[] = ". -";
        char *p = strtok(s1, token);

        while (p) {
            strncat(trouble, p, 3);
            p = strtok(NULL, token);
        }

        printf("%s ", trouble);

        return 0;
    }
    ```

2.
    ```c=
    int main() {
        int arr[] = {1, 3, 6, 2, 3, -3, 3, -2, 7, 9};
        int *p = arr;

        while (p < arr + sizeof(arr) / sizeof(int)) {
            printf("%d ", *p);
            p += *p;
        }
    }
    ```

3.
    ```c=
    #include <stdio.h>

    int i = 0;

    int main() {
        while (++i < 3) printf("%d ", i++);
        int i = 0;

        while (i++ < 3) printf("%d ", ++i);

        for (int i = 0; ++i < 10; i++)
            printf("%d ", i++);
        printf("%d", ++i);

        return 0;
    }
    ```

## B. Debug

1. 將 0~9, '.' 和 '%' 以外的字元都變成底線
    ```=
    #include <stdio.h>

    int isvalid(char c) {
        if (c == '.') return 1;
        if (c == '%') return 1;
        if (c > '0' && c < '9') return 1;
    }

    int main() {
        printf("How many sets of test data: ");
        int n;
        scanf("%d", n);
        getchar();

        while (n--) {
            char s[512];
            printf("Input a line:\n");
            fgets(s, 0, stdin);

            printf("Modified line:\n");
            char *p = s;
            while (p != "\n") {
                if (!isvalid(p)) printf("_");
                else putchar(p);
                *p++;
            } putchar('\n')
        }

        return 0;
    }
    ```

2. 使用一個日期的結構並比較他們的大小
    ```=
    #include <stdio.h>

    struct DateData {
        int yy, mm, dd;
    }

    void getdate(DateData d) {
        scanf("%d/%d/%d", d.yy, d.mm, d.dd);
    }

    int datecmp(DateData date1, DateData date2) {
        if (date1.yy > date2.yy) return  1;
        if (date1.yy < date2.yy) return -1;
        if (date1.mm > date2.mm) return  1;
        if (date1.mm < date2.mm) return -1;
        if (date1.dd > date2.dd) return  1;
        if (date1.dd < date2.dd) return -1;
        return 0;
    }

    int main() {
        struct DateData myDate1, myDate2;
        int ti, repeatTimes, rpTimes;

        printf("How many sets of test data: ");
        scanf("%d", &repeatTimes);

        for (ti = 0; ti < repeatTimes; ti++) {
            printf("\nInput the first date (yy/mm/dd): ");
            getdate(myDate1);
            printf("Input the second date (yy/mm/dd): ");
            getdate(myDate2);
            int result = datecmp(myDate1, myDate2);
            printf("%d-%02d-%02d", myDate1.yy, myDate1.mm, myDate1.dd);
            if (result == -1)
                printf(" > ");
            else if (result == 1)
                printf(" < ");
            else
                printf(" = ");
            printf("%d-%02d-%02d\n", myDate2.yy, myDate2.mm, myDate2.dd);
        }
        return 0;
    }
    ```

C. Write
1. 輸入一個數字，並以 32 位元顯示該數字的二進位
	```c=
	#include <stdio.h>

	void showBinary(int n) {
		// Your code here
	}

	int main() {
		int n;

		scanf("%d", &n);

		showBinary(n);

		return 0;
	}
	```

2. 以遞迴的方式完成最大公因數的 function
$GCD(a,\ b)=\begin{cases}b&,\ \text{if a is a multiple of b}\\GCD(b,\ a\ mod\ b) &,\ otherwise\\ \end{cases}$