#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#define width 20
#define height 10
char maze[2 * height + 1][2 * width + 1];

void print_maze();

void initmaze()
{
    int w, h;

    for(h = 0 ; h < 2 * height + 1 ; h++)
        for(w = 0 ; w < 2 * width + 1 ; w++)
        {
            if(h % 2 == 1 && w % 2 == 1)
                maze[h][w] = '?';
            else
                maze[h][w] = '#';
        }

    maze[1][0] = maze[1][1] = ' ';
}

void createmaze(int x, int y)
{
    int offsetX[4] = {-2, 2, 0, 0};
    int offsetY[4] = {0, 0, -2, 2};
    int count= 0, i, random;

    while(1)
    {
        count = 0;
        for(i = 0 ; i < 4 ; i++)
            if(maze[x + offsetX[i]][y + offsetY[i]] == '?')
                   count++;

       if(count == 0)
            return;

        do{
            random = rand() % 4;
        }while(x + offsetX[random] < 0 || x + offsetX[random] > 2 * height
                || y + offsetY[random] < 0 || y + offsetY[random] > 2 * width 
                || maze[x + offsetX[random]][y + offsetY[random]] != '?');

        maze[x + offsetX[random]][y + offsetY[random]] = maze[x + (offsetX[random] / 2)][y + (offsetY[random] / 2)] = ' ';

        print_maze();
        usleep(50000);
        printf("\033[%d;%dH", 1, 1); // Goto 1 1

        createmaze(x + offsetX[random], y + offsetY[random]);
    }
}

void print_maze()
{
    int w, h;

    for(h = 0 ; h < 2 * height + 1 ; h++)
    {
        for(w = 0 ; w < 2 * width + 1 ; w++)
        {
            if(maze[h][w] == '?')
                printf("#");
            else
                printf("%c", maze[h][w]);
        }

        printf("\n");
    }
}

int main()
{
    srand((unsigned int) time(NULL));

    initmaze();

    system("clear");
    system("tput civis"); // Hide cursor

    createmaze(1, 1);

    maze[19][40] = ' ';

    print_maze();
    system("tput cvvis"); // Show cursor

    return 0;
}
