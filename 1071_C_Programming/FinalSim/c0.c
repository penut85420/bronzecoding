#include <stdio.h>

void showBinary(int n) {
	for (int i = 31; i >= 0; i--) {
		int t = 1 << i;
		if (n & t) putchar('1');
		else putchar('0');
	} putchar('\n');
}

int main() {
	int n;

	scanf("%d", &n);

	showBinary(n);

	return 0;
}