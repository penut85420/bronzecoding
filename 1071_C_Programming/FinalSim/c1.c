#include <stdio.h>

int gcd(int a, int b) {
	if (a % b == 0) return b;
	return gcd(b, a % b);
}

int main() {
	int x, y;

	scanf("%d %d", &x, &y);
	printf("GCD(%d, %d) = %d\n", x, y, gcd(x, y));

	return 0;
}