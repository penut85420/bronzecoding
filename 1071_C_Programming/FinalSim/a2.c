#include <stdio.h>

int i = 0;

int main() {
    while (++i < 3) printf("%d ", i++);
    int i = 0;

    while (i++ < 3) printf("%d ", ++i);

    for (int i = 0; ++i < 10; i++)
        printf("%d ", i++);
    printf("%d\n", ++i);

    return 0;
}

// Output: 1 2 4 1 4 7 6