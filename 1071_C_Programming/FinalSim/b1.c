#include <stdio.h>

struct DateData {
    int yy, mm, dd;
};

void getdate(struct DateData *d) {
    scanf("%d/%d/%d", &d->yy, &d->mm, &d->dd);
}

int datecmp(struct DateData date1, struct DateData date2) {
    if (date1.yy > date2.yy) return  1;
    if (date1.yy < date2.yy) return -1;
    if (date1.mm > date2.mm) return  1;
    if (date1.mm < date2.mm) return -1;
    if (date1.dd > date2.dd) return  1;
    if (date1.dd < date2.dd) return -1;
    return 0;
}

int main() {
    struct DateData myDate1, myDate2;
    int ti, repeatTimes, rpTimes;

    printf("How many sets of test data: ");
    scanf("%d", &repeatTimes);

    for (ti = 0; ti < repeatTimes; ti++) {
        printf("\nInput the first date (yy/mm/dd): ");
        getdate(&myDate1);
        printf("Input the second date (yy/mm/dd): ");
        getdate(&myDate2);
        int result = datecmp(myDate1, myDate2);
        printf("%d-%02d-%02d", myDate1.yy, myDate1.mm, myDate1.dd);
        if (result == -1)
            printf(" > ");
        else if (result == 1)
            printf(" < ");
        else
            printf(" = ");
        printf("%d-%02d-%02d\n", myDate2.yy, myDate2.mm, myDate2.dd);
    }
    return 0;
}