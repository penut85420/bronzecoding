#include <stdio.h>

typedef struct {
	int score;
	int id;
	char name[32];
} studentInfo;

void getStudent(studentInfo*);
double countAvg(studentInfo[], int);
studentInfo* findMax(studentInfo[], int);

int main() {
	int i, n;
	studentInfo s[100], *p;

	printf("How many students: ");
	scanf("%d", &n);
	for (i = 0; i < n; i++)
		getStudent(&s[i]);

	p = findMax(s, n);

	printf(
		"\n[Mean] %.2lf\n"
		"[Max] Name: %s, ID: %d, Score: %d\n",
		countAvg(s, n), p->name, p->id, p->score);
	return 0;
}

void getStudent(studentInfo *s) {
	scanf("%s%d%d", s->name, &s->id, &s->score);
}

double countAvg(studentInfo s[], int n) {
	int i, sum = 0;
	for (i = 0; i < n; i++)
		sum += s[i].score;
	return sum * 1.0 / n;
}

studentInfo* findMax(studentInfo list[], int n) {
	studentInfo *max_s = list;
	for (int i = 0; i < n; i++)
		if (list[i].score > max_s->score)
			max_s = &list[i];
	return max_s;
}
