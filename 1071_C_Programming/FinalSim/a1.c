#include <stdio.h>

int main() {
    int arr[] = {1, 3, 6, 2, 3, -3, 3, -2, 7, 9};
    int *p = arr;

    while (p < arr + sizeof(arr) / sizeof(int)) {
        printf("%d ", *p);
        p += *p;
    }
}

// Output: 1 3 3 -2 -3 6 7