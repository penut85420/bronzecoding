#include <stdio.h>

int isvalid(char c) {
    if (c == '.') return 1;
    if (c == '%') return 1;
	if (c >= '0' && c <= '9') return 1;
    return 0;
}

int main() {
    printf("How many sets of test data: ");
    int n;
    scanf("%d", &n);
    getchar();

    while (n--) {
        char s[512];
        printf("\nInput a line:\n");
        fgets(s, 512, stdin);

        printf("Modified line:\n");
        char *p = s;
        while (*p != '\n') {
            if (!isvalid(*p)) printf("_");
            else putchar(*p);
            *p++;
        } putchar('\n');
    }

    return 0;
}