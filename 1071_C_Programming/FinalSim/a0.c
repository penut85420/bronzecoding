#include <stdio.h>
#include <string.h>

int main() {
    char s1[] = "You do not need to modified your program. "
        "The BasicPractice webpage version is exactly "
        "the same as the one on OnlineJudge or E-tutor.";
    char trouble[512] = { 0 };
    char token[] = ". -";
    char *p = strtok(s1, token);

    while (p) {
        strncat(trouble, p, 3);
        p = strtok(NULL, token);
    }

    printf("%s\n", trouble);

    return 0;
}

// Output: YoudonotneetomodyouproTheBaswebverisexathesamastheoneonOnlorEtut