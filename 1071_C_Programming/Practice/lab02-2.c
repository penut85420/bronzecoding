#include <stdio.h>

int main() {
	int firstPrize, invoiceNumber;

	printf("First prize number:  ");
	scanf("%d", &firstPrize);

	printf("Your invoice number: ");
	scanf("%d", &invoiceNumber);

	if (invoiceNumber == firstPrize)
		printf("Congratulations! You've won the first prize!\n");
	else if (invoiceNumber % 10000000 == firstPrize % 10000000)
		printf("Congratulations! You've won the second prize!\n");
	else if (invoiceNumber % 1000000  == firstPrize % 1000000)
		printf("Congratulations! You've won the third prize!\n");
	else if (invoiceNumber % 100000   == firstPrize % 100000)
		printf("Congratulations! You've won the fourth prize!\n");
	else if (invoiceNumber % 10000    == firstPrize % 10000)
		printf("Congratulations! You've won the fifth prize!\n");
	else if (invoiceNumber % 1000     == firstPrize % 1000)
		printf("Congratulations! You've won the sixth prize!\n");
	else 
		printf("Sorry. You did not win any prize.\n");

	return 0;
}