#include <stdio.h>
#include <string.h>

#define	IRONHEAD	1
#define	WHIRLLEG	2
#define	BELLSHIELD	4
#define	GHOSTHAND	8
#define	LIGHTBODY	16
#define	KINGKONGLEG	32
#define	LIONROAR	64
#define	RULAIPALM	128

char *KongfuNames[] = {
    "鐵頭功",
    "旋風地堂腿",
    "金鐘罩鐵布衫",
    "鬼影擒拿手",
    "輕功水上飄",
    "大力金剛腿",
    "獅子吼",
    "如來神掌"
}, KongfuNums = 8;

typedef struct {
    char name[30];
    int level;
    char skill;
} PlayerData;

void printMenu() {
    printf(
        "1. 儲存新學會功夫\n"
        "2. 查詢玩家學會的功夫\n"
        "3. 查詢學會功夫之玩家\n"
        "4. 離開\n"
        "請輸入動作："
    );
}

void showPlayer(PlayerData p) {
    printf(
        "Name: %s\nLevel: %d\nSkills:\n",
        p.name, p.level
    );
    for (int j = 0; j < 8; j++)
        if (p.skill & (1 << j))
            printf("\t%s\n", KongfuNames[j]);
}

char getcmd() {
    char c = getchar();
    while (c == '\r' || c == '\n') c = getchar();
    while (getchar() != '\n') ;
    return c;
}

int main() {
    FILE *finn = fopen("./playerData.txt.1", "r");
    char line[512], tmp[512], _name[] = "name=", _level[] = "level=", _skill[] = "skill=", _player[] = "[player]";
    PlayerData player[100]; int total = 0, i, j;

    while (fgets(line, sizeof(line), finn) != NULL) {
        strtok(line, "\r\n");
        if (!strcmp(line, _player)) total++;
        else if (!strncmp(line, _name, strlen(_name)))
            strcpy(player[total].name, line + strlen(_name));
        else if (!strncmp(line, _level, strlen(_level)))
            sscanf(line + strlen(_level), "%d", &player[total].level);
        else if (!strncmp(line, _skill, strlen(_skill))) {
            for (i = 0; i < KongfuNums; i++)
                if (!strcmp(line + strlen(_skill), KongfuNames[i])) break;
            if (i != KongfuNums) player[total].skill |= (1 << i);
        }
    } total++;

    printMenu();
    char c = getcmd();
    while (c != '4') {
        switch (c) {
        case '1':
            printf("請輸入玩家：");
            scanf("%s", line);
            printf("請輸入功夫：");
            scanf("%s", tmp);
            for (i = 0; i < total; i++) {
                if (!strcmp(player[i].name, line))
                    break;
            }
            for (j = 0; j < KongfuNums; j++) {
                if (!strcmp(KongfuNames[j], tmp))
                    break;
            }
            if (i == total) printf("沒這個人\n");
            else if (j == KongfuNums) printf("沒這招\n");
            else {
                player[i].skill |= (1 << j);
                printf("儲存%s學會%s\n", player[i].name, KongfuNames[j]);
            }
            break;
        case '2':
            printf("請輸入玩家：");
            scanf("%s", line);
            for (i = 0; i < total; i++) {
                if (!strcmp(player[i].name, line))
                    showPlayer(player[i]);
            }
            break;
        case '3':
            printf("請輸入功夫：");
            scanf("%s", tmp);
            for (j = 0; j < KongfuNums; j++) {
                if (!strcmp(KongfuNames[j], tmp))
                    break;
            }
            printf("學會這些功夫的玩家為：");
            for (i = 0; i < total; i++) {
                if (player[i].skill & (1 << j))
                    printf("%s ", player[i].name);
            }
            break;
        }
        putchar('\n');
        printMenu();
        c = getcmd();
    }

    return 0;
}