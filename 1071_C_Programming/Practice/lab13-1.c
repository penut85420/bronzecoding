#include <stdio.h>
#include <string.h>

#define countryTotal 21

int indexOfList(char str[], char *list[], int itemTotal) {
    for (int i = 0; i < itemTotal; i++)
        if (!strcmp(str, list[i])) return i;
    return -1;
}

int main() {
    char s[32];

    char *country[32] = {
        "China",
        "Japan",
        "Korea",
        "India",
        "Indonesia",
        "Malaysia",
        "Singapore",
        "Thailand",
        "Viet Nam",
        "Taiwan",
        "Mexico",
        "Brazil",
        "Canada",
        "USA",
        "the United Kingdom",
        "Italy",
        "Spain",
        "France",
        "Germany",
        "Australia",
        "New Zealand"
    };

    int population[countryTotal] = {
        1369436, 126795, 50074, 1295292, 254455,
        29902, 5507, 67726, 92423, 23434, 125386,
        206078, 35588, 319449, 64331, 59789, 46260,
        64121, 80646, 23622, 4495
    };
    printf("Input a line: ");
    fgets(s, 32, stdin);
    strtok(s, "\n");

    while (strcmp(s, "quit")) {
        int i = indexOfList(s, country, countryTotal);
        if (i < 0) printf("Sorry, we cannot find [%s] in our database.\n", s);
        else printf("The population of %s is %d thousands.\n", s, population[i]);
        printf("Input a line: ");
        fgets(s, 32, stdin);
        strtok(s, "\n");
    }
    return 0;
}