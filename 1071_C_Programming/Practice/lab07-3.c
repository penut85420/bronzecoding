#include <stdio.h>
#include <stdlib.h>

void printCard(int id) {
    char * suitSymbol[4] = {"♣", "♦", "♥", "♠"};
    char highRank[4] = "JQKA";

    int s = id / 13;
    int r = id % 13 + 2;

    printf("%s", suitSymbol[s]);
    if (r > 10) printf("%-2c ", highRank[r - 11]);
    else printf("%-2d ", r);
}

void shuffling(int a[], int N) {
    for (int i = 0; i < N; i++)
		a[i] = i;
	
	for (int i = 0; i < N; i++) {
		int r = rand() % N;
		int t = a[r];
		a[r] = a[i];
		a[i] = t;
	}
}

int isFlush(int cards[]) {
    for (int i = 1; i < 5; i++)
        if (cards[i-1] / 13 != cards[i] / 13)
            return 0;
    return 1;
}

int isStraight(int cards[]) {
    int tag[14] = {0};

    for (int i = 0; i < 5; i++)
        tag[cards[i] % 13] = 1;

    for (int i = 1; i < 14; i++) {
        tag[i] = tag[i-1] + tag[i];
        if (tag[i] == 5) return 1;
    }
    return 0;
}

int main() {
    int cards[52];
    for (int i = 0; i < 10; i++) {
        shuffling(cards, 52);
        for (int j = 0; j < 5; j++)
            printCard(cards[j]);
        printf("\n");
    }
    return 0;
}