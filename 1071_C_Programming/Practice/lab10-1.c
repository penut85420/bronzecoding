#include <stdio.h>

int A(int m, int n) {
    if (!m) return n + 1;
    if (!n && m > 0) return A(m-1, 1);
    return A(m-1, A(m, n-1));
}

int main() {
    for (int m = 0; m <= 3; m++) {
        for (int n = 0; n <= 9; n++) {
            // printf("A(%d, %d) = %d\n", m, n, A(m, n));
            printf("%5d", A(m, n));
        } printf("\n");
    }
    return 0;
}