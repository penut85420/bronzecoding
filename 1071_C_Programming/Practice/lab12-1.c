#include <stdio.h>

void encrypt(char[]);

int main() {
    char str1[100] = "A funny guy. @@";
    encrypt(str1);
    printf("Encrypted str1: %s\n", str1);
    printf("Input a line: ");
    fgets(str1, 100, stdin); // get a line from keyboard and save it in str1
    encrypt(str1);
    printf("Encrypted line: %s\n", str1);
    return 0;
}

void encrypt(char str[]) {
    char *p = str;
    while (*p) {
        if (*p >= 'A' && *p <= 'Z')
            *p = (*p - 'A' + 2) % 26 + 'A';
        else if (*p >= 'a' && *p <= 'z')
            *p = (*p - 'a' + 2) % 26 + 'a';
        else if (*p >= '0' && *p <= '9')
            *p = (*p - '0' + 9) % 10 + '0';
        p++;
    }
}