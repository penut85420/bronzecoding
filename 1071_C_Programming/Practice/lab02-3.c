#include <stdio.h>

int main() {
	int a, b, total = 0;

	printf("Please input the number of Product A: ");
	scanf("%d", &a);

	printf("Please input the number of Product B: ");
	scanf("%d", &b);

	if (a > b) {
		total += b * 39;
		a -= b;
		b = 0;
	} else {
		total += a * 39;
		b -= a;
		a = 0;
	}
	printf("%d %d %d\n", a, b, total);
	if (a) total += (a / 2) * 40 + (a % 2) * 25;
	if (b) total += (b / 2) * 32 + (b % 2) * 20;

	printf("Total price: $%d\n", total);

	return 0;
}