#include <stdio.h>
#include <string.h>

int indexOfList_icmp(char *str, char list[100][64], int size) {
    for (int i = 0; i < size; i++)
        if (!stricmp(str, list[i])) return i;
    return -1;
}

int main() {
    char document[]="National Taiwan Ocean University was originally established in 1953 as a junior college for the study of maritime science and technology.\nAfter eleven years, in 1964, we became a maritime college which offered bachelor's and master's degrees in various fields of maritime studies. During this period, funds for running the college came from the Taiwan Provincial Government of the Republic of China.\nIn 1979 the national government took over the funding and we became the National Maritime College. After another decade, in 1989, the college grew into a full-fledged university, National Taiwan Ocean University (NTOU).";
    char sp[] = " .,()\n\r'";
    char *p = strtok(document, sp);
    char list[100][64];
    int count[100] = {0}, size = 0;

    while (p != NULL) {
        int idx = indexOfList_icmp(p, list, size);
        if (idx == -1) {
            strcpy(list[size], p);
            idx = size;
            size++;
        } 
        count[idx]++;
        p = strtok(NULL, sp);
    }

    for (int i = 0; i < size; i++) {
        printf("%-24s%d\n", list[i], count[i]);
    }

    return 0;
}