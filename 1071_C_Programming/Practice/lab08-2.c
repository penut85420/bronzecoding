#include <stdio.h>

int main () {
    int i, j, n, arr[256];

    printf("How many numbers: ");
    scanf("%d", &n);
    printf("Before sorting: ");
    for (i = 0; i < n; i++)
        scanf("%d", &arr[i]);
    
    for (i = 0; i < n; i++) {
        int m = i;
        for (j = i + 1; j < n; j++) {
            if (arr[j] < arr[m])
                m = j;
        }
        int t = arr[m];
        arr[m] = arr[i];
        arr[i] = t;
    }

    printf("After sorting: ");
    for (i = 0; i < n; i++)
        printf("%d ", arr[i]);

    return 0;
}