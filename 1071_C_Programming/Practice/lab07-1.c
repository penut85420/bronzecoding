#include <stdio.h>
#include <stdlib.h>

void shuffling(int a[], int N) {
    for (int i = 0; i < N; i++)
		a[i] = i;
	
	for (int i = 0; i < N; i++) {
		int r = rand() % N;
		int t = a[r];
		a[r] = a[i];
		a[i] = t;
	}
}

int main() {
	printf("Permutation of how many numbers? ");
	int n;
	scanf("%d", &n);
	int arr[n];
	shuffling(arr, n);
	printf("Show how many numbers? ");
	scanf("%d", &n);
	printf("You have: ");
	for (int i = 0; i < n; i++)
		printf("%d ", arr[i]);

    return 0;
}