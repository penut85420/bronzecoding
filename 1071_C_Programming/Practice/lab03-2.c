#include <stdio.h>

int main() {
	int i, n;

	printf("Please input an integer: ");
	scanf("%d", &n);
	
	printf("Positive factors of %d are 1", n);
	if (n < 0) n = -n;

	for (i = 2; i <= n; i++)
		if (n % i == 0) printf(", %d", i);
	
	printf(".\n");

	return 0;
}