#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <windows.h>

#define width 20
#define height 10

char maze[2 * height + 1][2 * width + 1];
int offsetX[4] = {-2, 2, 0, 0};
int offsetY[4] = {0, 0, -2, 2};

void hideCursor() {
    HANDLE h = GetStdHandle(STD_OUTPUT_HANDLE);
    CONSOLE_CURSOR_INFO c = {1, FALSE};
    SetConsoleCursorInfo(h, &c);
}

void cls() {
    HANDLE h = GetStdHandle(STD_OUTPUT_HANDLE);
    COORD c = {0, 0};
    SetConsoleCursorPosition(h, c);
}

void initMaze() {
    for (int i = 0; i < 2 * height + 1; i++)
        for (int j = 0; j < 2 * width + 1; j++) {
            if (i % 2 && j % 2)
                maze[i][j] = '?';
            else maze[i][j] = '#';
        }
    maze[1][0] = ' ';
    maze[1][1] = ' ';
}

void printMaze() {
    cls();
    for (int i = 0; i < 2 * height + 1; i++) {
        for (int j = 0; j < 2 * width + 1; j++)
            printf("%c", maze[i][j] == ' '? ' ' : '#');
        printf("\n");
    }
    Sleep(100);
}

int inBound(int x, int y) {
    if (x < 0) return 0;
    if (y < 0) return 0;
    if (x > 2 * height) return 0;
    if (y > 2 * width) return 0;
    return 1;
}

void createMaze(int x, int y) {
    while (1) {
        int count = 0;
        for (int i = 0; i < 4; i++)
            if (maze[x + offsetX[i]][y + offsetY[i]] == '?')
                count++;
        if (!count) return;
        int r = rand() % 4;
        while (!inBound(x + offsetX[r], y + offsetY[r]) || maze[x + offsetX[r]][y + offsetY[r]] != '?')
            r = rand() % 4;
        maze[x + offsetX[r]/2][y + offsetY[r]/2] = ' ';
        printMaze();
        maze[x + offsetX[r]][y + offsetY[r]] = ' ';
        printMaze();
        createMaze(x + offsetX[r], y + offsetY[r]);
    }
}

int main() {
    system("title DFS Maze");
    
    printf("Any key to start!");
    getchar();
    hideCursor();
    system("cls");
    initMaze();
    printMaze();
    Sleep(900);
    srand(time(NULL));
    createMaze(1, 1);
    printMaze();
    maze[2 * height - 1][2 * width] = ' ';
    printMaze();
    printf("Done.");
    getchar();
    return 0;
}