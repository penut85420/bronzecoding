#include <stdio.h>

int main() {
    int choice;
    double input;
    double m2km = 1.609;

    printf("What kind of conversion do you want? (1 = miles to km; 2 = km to miles) ");
    scanf("%d", &choice);

    if (choice == 1) {
        printf("How many miles: ");
        scanf("%lf", &input);
        printf("%lf miles are equal to %lf kilometers.", input, input * m2km);
    } else if (choice == 2) {
        printf("How many kilometers: ");
        scanf("%lf", &input);
        printf("%lf kilometers are equal to %lf miles.", input, input / m2km);
    }
    
    return 0;
}