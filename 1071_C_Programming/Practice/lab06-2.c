#include <stdio.h>

int hasRepeatDigit(int);
int guessing();
int checkA(int, int);
int checkB(int, int);

int main() {
    int ans, guess;

    // Testing hasRepeatDigit()
    while (ans) {
        printf("Please guess a distinct-4-digit number: ");
        scanf("%d", &ans);
        if (hasRepeatDigit(ans)) 
                printf("It has repeated digits.\n");
        else printf("It has no repeated digits.\n");
    } 

    // Testing geussing()
    // printf("You guess %d.\n", guessing());

    // Testing checkA()
    // printf("Please input two numbers: ");
    // scanf("%d %d", &ans, &guess);
    // while (ans && guess) {
    //     printf("%dA\n", checkA(ans, guess));
    //     printf("Please input two numbers: ");
    //     scanf("%d %d", &ans, &guess);
    // }

    // Testing checkB()
    // printf("Please input two numbers: ");
    // scanf("%d %d", &ans, &guess);
    // while (ans && guess) {
    //     printf("%dB\n", checkB(ans, guess));
    //     printf("Please input two numbers: ");
    //     scanf("%d %d", &ans, &guess);
    // }
    
    return 0;
}

int hasRepeatDigit(int a) {
    int i, j;
    for (i = 1; i <= 1000; i *= 10) {
        for (j = 1; j <= 1000; j *= 10) {
            if (i == j) continue;
            if ((a / i) % 10 == (a / j) % 10)
                return 1;
        }
    }
    return 0;
}

int guessing() {
    int ans;
    
    printf("Please guess a distinct-4-digit number: ");
    scanf("%d", &ans);
    while (hasRepeatDigit(ans)) {
        printf("No repeated digits are allowed\n");
        printf("Please guess a distinct-4-digit number: ");
        scanf("%d", &ans);
    }

    return ans;
}

int checkA(int a, int b) {
    int i, A = 0;
    for (i = 1; i <= 1000; i *= 10) {
        if (a / i % 10 == b / i % 10) 
            A++;
    }
    return A;
}

int checkB(int a, int b) {
    int i, j, B = 0;

    for (i = 1; i <= 1000; i *= 10) {
        for (j = 1; j <= 1000; j *= 10) {
            if (i == j) continue;
            if (a / i % 10 == b / j % 10) 
                B++;
        }
    }

    return B;
}