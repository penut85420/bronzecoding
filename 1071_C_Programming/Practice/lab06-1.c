#include <stdio.h>

int main() {
    char * subject[] = {"國文", "英文", "數學"};
    int score[2][3][5][3] =  // score[year][class][seatNo][subject = {Chinese, English, Math}]
        { { {{98, 95, 92}, {89, 78, 82}, {88, 64, 64}, {61, 43, 44}, {48, 57, 58}},
            {{86, 78, 44}, {65, 65, 63}, {56, 67, 77}, {47, 78, 43}, {54, 56, 58}},
            {{46, 50, 76}, {65, 87, 66}, {64, 56, 66}, {92, 49, 86}, {45, 73, 83}} },
            
        { {{77, 52, 91}, {40, 45, 69}, {89, 70, 82}, {75, 94, 60}, {78, 86, 63}},
            {{85, 50, 91}, {92, 70, 82}, {72, 64, 93}, {86, 75, 52}, {43, 40, 83}},
    	    {{89, 92, 67}, {55, 61, 91}, {40, 54, 48}, {75, 79, 47}, {47, 44, 97}} } };
    int y, c, n, s;

    // Step 1
    // y = 0; c = 0;
    // for (s = 0; s < 3; s++) {
    //     int sum = 0;
    //     for (n = 0; n < 5; n++)
    //         sum += score[y][c][n][s];
    //     printf("%d年%d班學生%s成績總和為 %d\n", y+1, c+1, subject[s], sum);
    // }
    
    // Step 2
    // y = 0;
    // for (s = 0; s < 3; s++)
    //     for (c = 0; c < 3; c++) {
    //         int sum = 0;
    //         for (n = 0; n < 5; n++)
    //             sum += score[y][c][n][s];
    //         printf("%d年%d班學生%s成績總和為 %d\n", y+1, c+1, subject[s], sum);
    //     }

    // Step 3
    // for (y = 0; y < 2; y++)
    //     for (s = 0; s < 3; s++)
    //         for (c = 0; c < 3; c++) {
    //             int sum = 0;
    //             for (n = 0; n < 5; n++) 
    //                 sum += score[y][c][n][s];
    //             printf("%d年%d班學生%s成績總和為 %d\n", y+1, c+1, subject[s], sum);
    //         }

    // Step 4
    // for (y = 0; y < 2; y++)
    //     for (s = 0; s < 3; s++)
    //         for (c = 0; c < 3; c++) {
    //             int sum = 0;
    //             for (n = 0; n < 5; n++)
    //                 sum += score[y][c][n][s];
    //             printf("%d年%d班學生%s成績平均為 %.2lf\n", y+1, c+1, subject[s], sum/5.0);
    //         }

    // Step 5
    for (s = 0; s < 3; s++) {
        for (y = 0; y < 2; y++) {
            int sum = 0;
            for (c = 0; c < 3; c++) {
                for (n = 0; n < 5; n++)
                    sum += score[y][c][n][s];
            }
            printf("%d年級學生國文成績總和為 %d\n", y+1, sum);
        }
    }

    return 0;
}