#include <stdio.h>

int main() {
	const int QNUMBER1 = 21;
	const int QNUMBER2 = 35;
	int guessNumber;

	printf("Enter a common factor of %d and %d: ", QNUMBER1, QNUMBER2);
	scanf("%d", &guessNumber);

	if (!guessNumber) 
		printf("Please do not guess 0!!!!!\n");
	else {
		if (QNUMBER1 % guessNumber == 0 && QNUMBER2 % guessNumber == 0)
			printf("Correct! What a math genius!!\n");
		else printf("Incorrect! Have you ever learned math before?\n");
	}

	printf("Game over.");

	return 0;
}