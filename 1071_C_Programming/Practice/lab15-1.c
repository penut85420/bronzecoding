#include <stdio.h>
#include <string.h>

struct borrowerInfo {
    char borrowerID[12], name[20];
    int count;
};

struct bookInfo {
    char bookID[12], title[80], author[80];
    int count;
};

struct dateInfo {
    int year, month, day;
};

struct circulateInfo {
    struct borrowerInfo *borrower;
    struct bookInfo *book;
    struct dateInfo borrowDate, dueDate, returnDate;
};

void printCommands();

int main() {
    struct bookInfo book[50];
    struct borrowerInfo borrower[20];
    struct circulateInfo record[200];
    int bookTotal = 0, borrowerTotal = 0, recordTotal = 0;

    int c;
    printCommands();
    scanf("%d", &c); getchar();

    while (c) {
        if (c == 3) {
            printf("\nData of all borrowers:\n");
            for (int i = 0; i < borrowerTotal; i++)
                printf("Borrower ID: %s, Name: %s\n", borrower[i].borrowerID, borrower[i].name);
        } else if (c == 4) {
            printf("\nData of all books:\n");
            for (int i = 0; i < bookTotal; i++)
                printf("Book ID: %s\n  title: %s\nauthors: %s\n", book[i].bookID, book[i].title, book[i].author);
        } else if (c == 6) {
            printf("Borrower ID: ");
            fgets(borrower[borrowerTotal].borrowerID, sizeof(borrower[borrowerTotal].borrowerID), stdin);
            strtok(borrower[borrowerTotal].borrowerID, "\r\n");
            printf("Borrower Name: ");
            fgets(borrower[borrowerTotal].name, sizeof(borrower[borrowerTotal].name), stdin);
            strtok(borrower[borrowerTotal].name, "\r\n");
            borrower[borrowerTotal].count = 0;
            borrowerTotal++;
        } else if (c == 7) {
            printf("Book ID: ");
            fgets(book[bookTotal].bookID, sizeof(book[bookTotal].bookID), stdin);
            strtok(book[bookTotal].bookID, "\r\n");
            printf("Book title: ");
            fgets(book[bookTotal].title, sizeof(book[bookTotal].title), stdin);
            strtok(book[bookTotal].title, "\r\n");
            printf("Authors: ");
            fgets(book[bookTotal].author, sizeof(book[bookTotal].author), stdin);
            strtok(book[bookTotal].author, "\r\n");
            book[bookTotal].count = 1;
            bookTotal++;
        }
        printCommands();
        scanf("%d", &c); getchar();
    }
    printf("Thanks for using NTOU CSE Library Curculation System!!\n");
    return 0;
}

void printCommands() {
    printf(
        "\nMenu of NTOU CSE Library Circulation System\n"
        "1. Borrow a book.\n"
        "2. Return a book.\n"
        "3. Print all the borrowers.\n"
        "4. Print all the books.\n"
        "5. Print all the circulation records.\n"
        "6. Add a new borrower.\n"
        "7. Add a new book.\n"
        "0. Exit.\n"
        "Please choose one action: "
    );
}