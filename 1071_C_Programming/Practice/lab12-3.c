#include <stdio.h>

void delSpace(char str[]) {
    char *p = str;
    while (*p) p++;
    p--;
    while (*p == ' ') p--;
    *(p + 1) = 0;

    char *q = str; p = str;
    while (*q == ' ') q++;
    while (*q) *p++ = *q++;
    *p = 0;
}

int main() {
    char str1[] = "Test    string    1";
    char str2[] = "   Test string    2   ";
    char str3[] = "    ";
    printf("Before:\t[%s]\n\t[%s]\n\t[%s]\n", str1, str2, str3);

    delSpace(str1);
    delSpace(str2);
    delSpace(str3);

    printf("\nAfter:\t[%s]\n\t[%s]\n\t[%s]\n", str1, str2, str3);
    return 0;
}