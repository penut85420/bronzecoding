#include <stdio.h>
#include <stdlib.h>

void printCard(int id) {
    char * suitSymbol[4] = {"♣", "♦", "♥", "♠"};
    char highRank[4] = "JQKA";

    int s = id / 13;
    int r = id % 13 + 2;

    printf("%s", suitSymbol[s]);
    if (r > 10) printf("%-2c ", highRank[r - 11]);
    else printf("%-2d ", r);
}

void shuffling(int a[], int N) {
    for (int i = 0; i < N; i++)
		a[i] = i;
	
	for (int i = 0; i < N; i++) {
		int r = rand() % N;
		int t = a[r];
		a[r] = a[i];
		a[i] = t;
	}
}

int main() {
    int cards[52];

    shuffling(cards, 52);

    for (int i = 0, n = 0; i < 4; i++) {
        printf("Player %d: ", i + 1);
        for (int j = 0; j < 13; j++) {
            printCard(cards[n++]);
        }
        printf("\n");
    }

    return 0;
}