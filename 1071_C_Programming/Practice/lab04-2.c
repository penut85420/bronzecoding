#include <stdio.h>
#include <math.h>

int main() {
	int k = 1;
	double x, t, term[512], sum = 0;

	printf("Please input the value of x: ");
	scanf("%lf", &x);
	printf("Please input the value of tolerance: ");
	scanf("%lf", &t);

	term[k] = x;

	do {
		term[k + 2] = term[k] * x * x / (k+1) / (k+2);
		k += 2;
		if (k/2%2) sum -= term[k];
		else sum += term[k];
	} while (term[k] > t);

	printf("sin(%.6lf) ~= %.20lf\n", x, sum + x);
	printf("sin(%.6lf) == %.20lf\n", x, sin(x));

	return 0;
}