#include <stdio.h>

int main() {
	int bgDay[12] = {
		20, 20, 21, 21,
		21, 22, 23, 23,
		23, 23, 22, 22
	};

	char *name[12] = {
		"Aries", "Taurus", "Gemini", "Cancer",
		"Leo", "Virgo", "Libra", "Scorpio",
		"Sagittarius", "Capricorn", "Aquarius", "Pisces",
	};

	int mm, dd;

	printf("Enter your birthday (in the format of month/day): ");
	scanf("%d/%d", &mm, &dd);

	if (dd < bgDay[--mm]) mm = (mm + 11) % 12;
	mm = (mm + 10) % 12;
	printf("You are %s. Its zodiac code is %d.\n", name[mm], mm);

	return 0;
}