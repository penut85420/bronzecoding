#include <stdio.h>
#include <stdlib.h>

typedef struct {
    int yy, mm, dd;
} DateData;

typedef struct {
    char ID[13], nickname[31];
    int loginTimes, articles, money;
    DateData lastLogin;
    char gender, flags;
} BloggerData;

void printProfile(BloggerData *b) {
    printf("\n[帳號] %s [暱稱] %s [上站] %d 次 [文章] %d 篇\n", b->ID, b->nickname, b->loginTimes, b->articles);
    printf("[認證] %s ", (b->flags & 1)? "已經通過認證": "尚未通過認證");
    printf("[上次] %4d 年 %2d 月 %2d 日 ", b->lastLogin.yy, b->lastLogin.mm, b->lastLogin.dd);
    printf("[性別] 我是");
    switch (b->gender) {
    case 'B':
        printf("帥哥");
        break;
    case 'G':
        printf("美女");
        break;
    case 'A':
        printf("動物");
        break;
    case 'R':
        printf("礦物");
        break;
    default:
        printf("未知");
    }
    printf(" [財產] ");
    if (b->money < 0) printf("負債累累");
    else if (b->money < 1000) printf("家境清寒");
    else if (b->money < 10000) printf("家境普通");
    else if (b->money < 20000) printf("家境小康");
    else if (b->money < 30000) printf("家境富有");
    else printf("家財萬貫");
    printf("\n");
}

int main() {
    FILE *fin = fopen("bloggerData_UTF8.binary", "rb");
    int i, total;
    BloggerData *b;

    fseek(fin, 0, SEEK_END);
    total = ftell(fin) / sizeof(BloggerData);
    printf("總共讀進 %d 位部落客資料。\n", total);

    b = (BloggerData*)malloc(sizeof(BloggerData) * total);
    fseek(fin, 0, SEEK_SET);
    fread(b, sizeof(BloggerData), total, fin);

    for (i = 0; i < total; i++)
        printProfile(b + i);

    fclose(fin);
    
    return 0;
}