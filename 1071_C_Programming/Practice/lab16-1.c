#include <stdio.h>
#include <string.h>

#define	IRONHEAD	1
#define	WHIRLLEG	2
#define	BELLSHIELD	4
#define	GHOSTHAND	8
#define	LIGHTBODY	16
#define	KINGKONGLEG	32
#define	LIONROAR	64
#define	RULAIPALM	128

char *KongfuNames[] = {
    "鐵頭功",
    "旋風地堂腿",
    "金鐘罩鐵布衫",
    "鬼影擒拿手",
    "輕功水上飄",
    "大力金剛腿",
    "獅子吼",
    "如來神掌"
}, KongfuNums = 8;

typedef struct {
    char name[30];
    int level;
    char skill;
} PlayerData;

int main() {
    FILE *finn = fopen("./playerData.txt.1", "r");
    char line[512], _name[] = "name=", _level[] = "level=", _skill[] = "skill=", _player[] = "[player]";
    PlayerData player[100]; int total = -1, i;

    while (fgets(line, sizeof(line), finn) != NULL) {
        strtok(line, "\r\n");
        if (!strcmp(line, _player)) total++;
        else if (!strncmp(line, _name, strlen(_name)))
            strcpy(player[total].name, line + strlen(_name));
        else if (!strncmp(line, _level, strlen(_level)))
            sscanf(line + strlen(_level), "%d", &player[total].level);
        else if (!strncmp(line, _skill, strlen(_skill))) {
            for (i = 0; i < KongfuNums; i++)
                if (!strcmp(line + strlen(_skill), KongfuNames[i])) break;
            if (i != KongfuNums) player[total].skill |= (1 << i);
        }
    } total++;

    for (i = 0; i < total; i++) {
        printf(
            "Name: %s\nLevel: %d\nSkills:\n",
            player[i].name, player[i].level
        );
        for (int j = 0; j < 8; j++)
            if (player[i].skill & (1 << j))
                printf("\t%s\n", KongfuNames[j]);
    }

    return 0;
}