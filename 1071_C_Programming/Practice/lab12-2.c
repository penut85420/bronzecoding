#include <stdio.h>

int main() {
    int base, n = 0;
    char a;
    printf("Please input the base: ");
    scanf("%d", &base); getchar();
    printf("Please input a number: ");
    a = getchar();
    while (a != '\n') {
        if (a >= 'A' && a <= 'Z')
            a = a - 'A' + 10;
        else if (a >= 'a' && a <= 'z')
            a = a - 'a' + 10;
        else a -= '0';
        if (a >= base) break;
        n = n * base + a;
        printf("%d ", n);
        a = getchar();
    }
    printf("This number is %d in decimal form.", n);
    return 0;
}