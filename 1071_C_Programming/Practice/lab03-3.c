#include <stdio.h>

int main() {
	int i;
	int totalDays, startDay;

	printf("Enter the number of days in month: ");
	scanf("%d", &totalDays);

	printf("Enter starting day of the week (0=Sunday, 1=Monday, ...) ");
	scanf("%d", &startDay);

	printf(" S  M  T  W  T  F  S\n");
	for (i = 0; i < startDay; i++)
		printf("   ");
	
	for (i = 1; i <= totalDays; i++) {
		printf("%2d ", i);
		startDay = (startDay + 1) % 7;
		if (!startDay) printf("\n");
	}

	return 0;
}