#include <stdio.h>
#include <stdlib.h>

typedef struct {
    int yy, mm, dd;
} DateData;

typedef struct {
    char ID[13], nickname[31];
    int loginTimes, articles, money;
    DateData lastLogin;
    char gender, flags;
} BloggerData;

int main() {
    FILE *fin = fopen("bloggerData_UTF8.binary", "rb");
    int i, total;
    BloggerData *b;

    fseek(fin, 0, SEEK_END);
    total = ftell(fin) / sizeof(BloggerData);
    printf("總共讀進 %d 位部落客資料。\n", total);

    b = (BloggerData*)malloc(sizeof(BloggerData) * total);
    fseek(fin, 0, SEEK_SET);
    fread(b, sizeof(BloggerData), total, fin);

    for (i = 0; i < total; i++)
        printf(
            "\nBlogger#%d\n"
            "ID = %s\n"
            "nickname = %s\n"
            "loginTimes = %d\n"
            "articles = %d\n"
            "money = %d\n"
            "lastLogin = %d\n"
            "gender = %c\n"
            "flags = %d\n",
            i, b[i].ID, b[i].nickname, b[i].loginTimes, b[i].articles, 
            b[i].money, b[i].lastLogin.yy, b[i].gender, b[i].flags
        );

    fclose(fin);
    
    return 0;
}