#include <stdio.h>
#include <string.h>

char * strstrL(char *sent, char *list[], int size) {
    if (!*sent) return NULL;
    // printf("[%s]\n", sent);
    char *p = strstr(sent, list[0]);
    for (int i = 1; i < size; i++) {
        // printf("[%s]\n", list[i]);
        char *q = strstr(sent, list[i]);
        if ((q && q < p) || (q && !p)) p = q;
        // printf("%X\n", p);
    }
    return p;
}

int main() {
    char sentence1[300] = "英國1名女大生有天掉了提款卡、錢包，沒錢搭車回家，卻遇到1名「街友英雄」要用身上僅剩的3英鎊解救她。";
    char sentence2[300] = "國際油價今年下半年以來，跌幅高達46％，幾近腰斬，但物價仍漲個不停，「回不去了」。";
    char * punct[4] = {"「", "」", "，", "。"};
    int punctLen = strlen(punct[0]);

    char *p = strstrL(sentence1, punct, 4);
    char *q = sentence1;
    while (p) {
        *p = 0;
        printf("%s\n", q);
        q = p += punctLen;
        p = strstrL(p, punct, 4);
    } putchar('\n');

    p = strstrL(sentence2, punct, 4);
    q = sentence2;
    while (p) {
        *p = 0;
        printf("%s**", q);
        q = p += punctLen;
        p = strstrL(p, punct, 4);
    } printf("%s\n", q);

    return 0;
}