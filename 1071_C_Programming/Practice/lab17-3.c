#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    int yy, mm, dd;
} DateData;

typedef struct {
    char ID[13], nickname[31];
    int loginTimes, articles, money;
    DateData lastLogin;
    char gender, flags;
} BloggerData;

void printProfile(BloggerData *b) {
    printf("\n[帳號] %s [暱稱] %s [上站] %d 次 [文章] %d 篇\n", b->ID, b->nickname, b->loginTimes, b->articles);
    printf("[認證] %s ", (b->flags & 1)? "已經通過認證": "尚未通過認證");
    printf("[上次] %4d 年 %2d 月 %2d 日 ", b->lastLogin.yy, b->lastLogin.mm, b->lastLogin.dd);
    printf("[性別] 我是");
    switch (b->gender) {
    case 'B':
        printf("帥哥");
        break;
    case 'G':
        printf("美女");
        break;
    case 'A':
        printf("動物");
        break;
    case 'R':
        printf("礦物");
        break;
    default:
        printf("未知");
    }
    printf(" [財產] ");
    if (b->money < 0) printf("負債累累");
    else if (b->money < 1000) printf("家境清寒");
    else if (b->money < 10000) printf("家境普通");
    else if (b->money < 20000) printf("家境小康");
    else if (b->money < 30000) printf("家境富有");
    else printf("家財萬貫");
    printf("\n");
}

void getDate(DateData *d) {
    scanf("%d/%d/%d", &d->yy, &d->mm, &d->dd);
}

void printMenu() {
    printf(
        "\n【BBS 功能選單】\n"
        "1. 登入\n"
        "2. 發表文章\n"
        "3. 印出個人資料\n"
        "4. 查詢網友資料\n"
        "5. 登出\n"
        "0. 關閉 BBS 系統\n"
        "請選擇動作："
    );
}

char getCmd() {
    char c = getchar();
    while (c == '\n' || c == '\r') c = getchar();
    while (getchar() != '\n');
    return c;
}

int main() {
    FILE *fin = fopen("bloggerData_UTF8.binary", "rb");
    int i, total;
    char cmd, in[512];
    DateData today;
    BloggerData *b, *login, *view;

    fseek(fin, 0, SEEK_END);
    total = ftell(fin) / sizeof(BloggerData);

    b = (BloggerData*)malloc(sizeof(BloggerData) * total);
    fseek(fin, 0, SEEK_SET);
    fread(b, sizeof(BloggerData), total, fin);
    fclose(fin);

    printf("開啟 BBS 系統... 請輸入今天日期 (年/月/日)：");
    getDate(&today);

    printMenu();
    cmd = getCmd();

    while (cmd != '0') {
        if (cmd == '1') {
            printf("\n請輸入帳號：");
            scanf("%s", in);
            for (i = 0; i < total; i++)
                if (!strcmp(b[i].ID, in)) {
                    login = b + i;
                    break;
                }
            if (i != total) {
                login->loginTimes++;
                login->lastLogin = today;
                printf("\n%s 您好，這是您第 %d 次上站\n", login->nickname, login->loginTimes);
            }
        } else if (cmd == '2') {
            login->articles++;
            printf("\n您已發表第 %d 篇文章\n", login->articles);
        } else if (cmd == '3') {
            printProfile(login);
        } else if (cmd == '4') {
            printf("\n請輸入帳號：");
            scanf("%s", in);
            for (i = 0; i < total; i++)
                if (!strcmp(b[i].ID, in)) {
                    view = b + i;
                    break;
                }
            if (i != total) printProfile(view);
        } else if (cmd == '5') {
            printf("\n再見！下次再來玩！\n%s 已登出。\n", login->ID);
        }

        printMenu();
        cmd = getCmd();
    }

    FILE *fout = fopen("bloggerData_UTF8.binary", "wb");
    fwrite(b, sizeof(BloggerData), total, fout);
    fclose(fout);

    printf("\n關閉 BBS 系統... OK!\n");
    return 0;
}