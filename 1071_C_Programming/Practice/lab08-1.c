#include <stdio.h>
#include <stdlib.h>

int main () {
    double a, b;

    for (unsigned int i = 1000; i <= 1000000000; i *= 10) {
        int t = 0;
        for (unsigned int j = 0; j < i; j++) {
            a = 1.0 * rand() / RAND_MAX;
            b = 1.0 * rand() / RAND_MAX;
            if (a * a + b * b <= 1) t++;
        }
        printf("After %d points, PI = %lf\n", i, 4.0 * t / i);
    }

    return 0;
}