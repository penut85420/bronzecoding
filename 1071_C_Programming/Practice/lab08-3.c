#include <stdio.h>

void ssort(int arr[], int n) {
    for (int i = 0; i < n; i++) {
        int m = i;
        for (int j = i + 1; j < n; j++) {
            if (arr[j] < arr[m])
                m = j;
        }
        int t = arr[m];
        arr[m] = arr[i];
        arr[i] = t;
    }
}

int checkPrize(int ticket[], int first[], int special) {
    ssort(ticket, 6);
    ssort(first, 6);
    int mf = 0, ms = 0;

    for (int i = 0; i < 6; i++)
    for (int j = 0; j < 6; j++) {
        if (ticket[i] == first[j]) mf++;
        if (ticket[i] == special) ms = 1;
    }
    printf("%d %d\n", mf, ms);
    if (mf == 6) return 8;
    if (mf == 5 && ms) return 7;
    if (mf == 5) return 6;
    if (mf == 4 && ms) return 5;
    if (mf == 4) return 4;
    if (mf == 3 && ms) return 3;
    if (mf == 2 && ms) return 2;
    if (mf == 3) return 1;
    return 0;
}

int main() {
    char *prizeName[9] = {"", "General", "Seventh", "Sixth", "Fifth", "Fourth", "Third", "Second", "First"};
    int prize[9] = {0, 400, 400, 1000, 1286, 9781, 32705, 1569878, 19100192};
    int first[6], ticket[6], special, i;

    printf("Input the first-prize numbers: ");
    for (i = 0; i < 6; i++) scanf("%d", &first[i]);
    printf("Input the special number: ");
    scanf("%d", &special);
    printf("Input the numbers on the lottery ticket: ");
    for (i = 0; i < 6; i++) scanf("%d", &ticket[i]);

    int prizeCode = checkPrize(ticket, first, special);
    if (prizeCode > 0) printf("You have won the %s Prize which is NT$%d!!\n", prizeName[prizeCode], prize[prizeCode]);
    else printf("You did not win any prize.\n");
    return 0;
}