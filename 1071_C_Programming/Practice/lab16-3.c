#include <stdio.h>
#include <string.h>

#define lineTotal 4

int isIn(char, char*);
char *mystrtok(char*, char*);

int main() {
    char testline[lineTotal][100] = {
        "... Yes you got it!!! \\^^/",
        ".,?!///\\\\!!? ..",
        "",
        "A normal test. Hope it works!"
    };
    char brk[] = "., \\/?!";
    int i; char line[100], *token;
    for (i = 0; i < lineTotal; i++) {
        printf("Testing [%s] by strtok:\n", testline[i]);
        strcpy(line, testline[i]);
        token = strtok(line, brk);
        while (token != NULL) {
            printf("\t[%s]\n", token);
            token = strtok(NULL, brk);
        }
        printf("Testing [%s] by mystrtok:\n", testline[i]);
        strcpy(line, testline[i]);
        token = mystrtok(line, brk);
        while (token != NULL) {
            printf("\t[%s]\n", token);
            token = mystrtok(NULL, brk);
        }
    }
    return 0;
}

int isIn(char c, char *s) {
    for (int i = 0; i < strlen(s); i++)
        if (c == s[i]) return 1;
    return 0;
}

char *mystrtok(char *str, char *brk) {
    static char *nextp = NULL;
    char *token;

    if (str != NULL) nextp = str;

    if (nextp != NULL) {
        if (*nextp == 0) nextp = NULL;
        else
            while (isIn(*nextp, brk))
                nextp++;
    }

    if (nextp == NULL) return NULL;
    token = nextp;
    while (!isIn(*nextp, brk) && *nextp) nextp++;
    if (*nextp == 0) nextp = NULL;
    if (nextp == NULL) return NULL;
    while (isIn(*nextp, brk)) *nextp++ = 0;
    
    return token;
}
