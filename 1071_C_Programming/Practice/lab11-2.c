#include <stdio.h>

void printArray(int[], int);
void strangeFunc(int[], int);

int main() {
    int a[13] = {65, 0, -5, -22, 9, 231, 43, -7, 11, -65, 0, -18, 65};
    int b[12] = {65, 0, -5, -22, 9, 231, 43, -7, 11, 65, 0, -18};
    int c[12] = {65, 0, 5, 22, 9, 231, 43, 7, 11, 65, 0, 18};

    printf("Original a[] is "); printArray(a, 13);
    printf("Original b[] is "); printArray(b, 12);
    printf("Original c[] is "); printArray(c, 12);

    strangeFunc(a, 13);
    strangeFunc(b, 12);
    strangeFunc(c, 12);

    printf("\nModified a[] is "); printArray(a, 13);
    printf("Modified b[] is "); printArray(b, 12);
    printf("Modified c[] is "); printArray(c, 12);

    return 0;
}

void printArray(int arr[], int n) {
    printf("{%d", arr[0]);
    for (int i = 1; i < n; i++)
        printf(", %d", arr[i]);
    printf("}\n");
}

void strangeFunc(int a[], int n) {
    int *p = a, *q = a + n - 1;
    while (p < q) {
        while (*p >= 0 && p < q) p++;
        while (*q >= 0 && q > p) q--;
        int t = *p;
        *p = *q;
        *q = t;
        p++; q--;
    }
}