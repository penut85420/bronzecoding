#include <stdio.h>

int main() {
	int n, t = 0;

	printf("Guess a number whose remainders are 2, 3, and 2 when divided by 3, 5, and 7, respectively: ");
	scanf("%d", &n); t++;

	while (!(n % 3 == 2 && n % 5 == 3 && n % 7 == 2)) {
		printf("Incorrect!\n");
		printf("Guess a number whose remainders are 2, 3, and 2 when divided by 3, 5, and 7, respectively: ");
		scanf("%d", &n); t++;
	}

	printf("Correct!\n");

	if (t == 1) printf("Wow! Correct in the first time! Good job!");
	else if (t <= 5) printf("It took you %d times to answer correctly. You can do better.", t);
	else printf("It took you %d times to answer correctly. No one is better than you.", t);

	return 0;
}