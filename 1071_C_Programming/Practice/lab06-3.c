#include <stdio.h>
#include <stdlib.h>

int hasRepeatDigit(int);
int guessing();
int checkA(int, int);
int checkB(int, int);
int settingHiddenAnswer();

int main() {
    int ans, guess, chance = 6;

    ans = settingHiddenAnswer();
    while (hasRepeatDigit(ans))
        ans = settingHiddenAnswer();
    printf("%.4d\n", ans);
    
    printf("You have %d chances to guess the number\n", chance);
    while (chance) {
        printf("Please guess a distinct-4-digit number: ");
        scanf("%d", &guess);
        if (hasRepeatDigit(guess)) {
            printf("You can only guess a 4-digit number.\n");
            continue;
        }
        int A = checkA(ans, guess), B = checkB(ans, guess);
        printf("%dA%dB\n", A, B);
        if (A == 4) break;
        chance--;
    }

    if (chance) printf("Congratulations!\n");
    else printf("Times up! You lose!\n");
    
    return 0;
}

int hasRepeatDigit(int a) {
    int i, j;
    for (i = 1; i <= 1000; i *= 10) {
        for (j = 1; j <= 1000; j *= 10) {
            if (i == j) continue;
            if ((a / i) % 10 == (a / j) % 10)
                return 1;
        }
    }
    return 0;
}

int guessing() {
    int ans;
    
    printf("Please guess a distinct-4-digit number: ");
    scanf("%d", &ans);
    while (hasRepeatDigit(ans)) {
        printf("No repeated digits are allowed\n");
        printf("Please guess a distinct-4-digit number: ");
        scanf("%d", &ans);
    }

    return ans;
}

int checkA(int a, int b) {
    int i, A = 0;
    for (i = 1; i <= 1000; i *= 10) {
        if (a / i % 10 == b / i % 10) 
            A++;
    }
    return A;
}

int checkB(int a, int b) {
    int i, j, B = 0;

    for (i = 1; i <= 1000; i *= 10) {
        for (j = 1; j <= 1000; j *= 10) {
            if (i == j) continue;
            if (a / i % 10 == b / j % 10) 
                B++;
        }
    }

    return B;
}

int settingHiddenAnswer() {
    return rand() % 10000;
}