#include <stdio.h>
#include <stdlib.h>

void mainMenu();

void specialMenu();
void mailMenu();
void talkMenu();


void gameMenu();
void lifeMenu();

void guessNumService();
void converterService();
void astrologyService();

int main() {
    system("chcp 65001 & cls");
    mainMenu();
    return 0;
}

void mainMenu() {
    while (1) {
        system("cls");
        printf("	(A)nnounce  ξ 精華公佈欄 ξ\n");
        printf("	(B)oards    Ω 佈告討論區 Ω\n");
        printf("	(C)lass     φ 分組討論集 φ\n");
        printf("	(F)avorite  η 我的最愛群 η\n");
        printf("	(M)ail      μ 信件典藏盒 μ\n");
        printf("	(T)alk      ω 休閒聊天地 ω\n");
        printf("	(U)ser      π 個人工具坊 π\n");
        printf("	(X)yz       θ 特殊招待所 θ\n");
        printf("	(G)oodbye   δ 下次再會吧 δ\n");
        printf("請選擇動作：");
        char ch = getchar() | 32;
        switch (ch) {
            case 'x':
                specialMenu();
                break;
            case 't':
                talkMenu();
                break;
            case 'm':
                mailMenu();
                break;
            case 'g': 
                return ;
        }
    }
}

void specialMenu() {
    while (1) {
        system("cls");
        printf("    (L)ife       【 生活服務 】\n");
        printf("    (K)TV        【 真情點歌 】\n");
        printf("    (J)oin       【 看板連署 】\n");
        printf("    (G)ame       【 遊戲人生 】\n");
        printf("    (M)arket     【 金融市場 】\n");
        printf("    (O)ther      【 雜七雜八 】\n");
        printf("    (Q)uit       【 回上一層 】\n");
        printf("請選擇動作：");
        char ch = getchar() | 32;
        switch (ch) {
            case 'g':
                gameMenu();
                break;
            case 'l':
                lifeMenu();
                break;
            case 'q': 
                return ;
        }
    }
}

void mailMenu() {
    while (1) {
        system("cls");
        printf("    (R)ead       ├ 閱讀信件 ┤\n");
        printf("    (M)ail       ├ 站內寄信 ┤\n");
        printf("    (L)ist       ├ 群組寄信 ┤\n");
        printf("    (I)nternet   ├ 寄依妹兒 ┤\n");
        printf("    (Z)ip        ├ 打包資料 ┤\n");
        printf("    (Y)es Sir!   ├ 投書站長 ┤\n");
        printf("    (Q)uit       ├ 回上一層 ┤\n");
        printf("請選擇動作：");
        char ch = getchar() | 32;
        switch (ch) {
            case 'q': 
                return ;
        }
    }
}

void talkMenu() {
    while (1) {
        system("cls");
        printf("    (U)sers      → 遊客名單 ←\n");
        printf("    (L)istMenu   → 設定名單 ←\n");
        printf("    (P)ager      → 切換呼叫 ←\n");
        printf("    (I)nvis      → 隱身密法 ←\n");
        printf("    (Q)uery      → 查詢網友 ←\n");
        printf("    (T)alk       → 情話綿綿 ←\n");
        printf("    (C)hatRoom   → 眾口鑠金 ←\n");
        printf("    (D)isplay    → 瀏覽水球 ←\n");
        printf("    (W)rite      → 回顧水球 ←\n");
        printf("    (Q)uit       → 回上一層 ←\n");
        printf("請選擇動作：");
        char ch = getchar() | 32;
        switch (ch) {
            case 'q': 
                return ;
        }
    }
}

void gameMenu() {
    while (1) {
        system("cls");
        printf("    (1)GuessNum  【  猜數字  】\n");
        printf("    (2)Game      【 遊戲樂園 】\n");
        printf("    (3)Game      【 反斗特區 】\n");
        printf("    (Q)uit       【 回上一層 \n");
        printf("請選擇動作：");
        char ch = getchar() | 32;
        switch (ch) {
            case '1':
                guessNumService();
                break;
            case 'q': 
                return ;
        }
    }
}

void lifeMenu() {
    while (1) {
        system("cls");
        printf("    (C)onverter  【 單位換算 】\n");
        printf("    (A)strology  【 星座運勢 】\n");
        printf("    (Q)uit       【 回上一層 】\n");
        printf("請選擇動作：");
        char ch = getchar() | 32;
        switch (ch) {
            case 'c':
                converterService();
                break;
            case 'a':
                astrologyService();
                break;
            case 'q': 
                return ;
        }
    }
}

void guessNumService() {
    int n;
    printf("猜我現在左手比的數字是多少? ");
    scanf("%d", &n);
    printf("猜錯了，真蠢!!");
    system("pause");
}

void converterService() {
    char * str[] = {"公里", "英哩"};
    double dd[] = {0.6215, 1.609};
    int i = 0; double n;
    printf("(1) 公里 => 英哩\n");
    printf("(2) 英哩 => 公里\n");
    while (i < 1 || i > 2) {
        printf("您想換算什麼單位? ");
        scanf("%d", &i);
    } i--;
    printf("請輸入%s數：", str[i]);
    scanf("%lf", &n);
    printf("等於 %.6lf %s\n", n * dd[i], str[i]);
    system("pause");
}

void astrologyService() {
    int n;
    printf("請輸入生日 (月/日): ");
    scanf("%d/%d", &n, &n);
    printf("我管你什麼星座，反正你下周運勢不差啦~\n");
    system("pause");
}