#include <stdio.h>

int C(int n, int k) {
    if (k == 0) return 1;
    if (k == n) return 1;
    return C(n-1, k-1) + C(n-1, k);
}

int main() {
    for (int m = 0; m <= 9; m++) {
        for (int n = 0; n <= m; n++) {
            // printf("A(%d, %d) = %d\n", m, n, A(m, n));
            printf("%5d", C(m, n));
        } printf("\n");
    }
    return 0;
}