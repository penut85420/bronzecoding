#include <stdio.h>
#define n 80000

int main() {
	int arr[n] = {0}, i, j;

	for (i = 1; i < n; i++) {
		for (j = 1; j < i; j++)
			if (i % j == 0) arr[i] += j;
	}

	for (i = 0; i < n; i++)
		if (i == arr[arr[i]] && i < arr[i])
			printf("%d, %d\n", i, arr[i]);

	return 0;
}