#include <stdio.h>

int main() {
	int n, b, m;

	printf("Enter a non-negative integer: ");
	scanf("%d", &n);

	printf("Enter the base: ");
	scanf("%d", &b);

	printf("In base-%d system: ", b);

	m = 1;
	while (m < n) m *= b;
	m /= b;
	
	while (m >= 1) {
		printf("%d", n / m);
		n %= m;
		m /= b;
	}

	return 0;
}