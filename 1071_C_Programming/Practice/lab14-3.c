#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define MONSTER_TOTAL 3

struct PlayerData {
    char name[64];
    int exp, hp, mp, spd, atk;
};

struct MonsterData {
    char type[64];
    int hp, spd, atk, provide_exp;
};

void initialPlayer(struct PlayerData*);
void initialMonster(struct MonsterData*);
void printPlayerData(struct PlayerData);

void attackP2M(struct PlayerData*, struct MonsterData*);
void attackM2P(struct PlayerData*, struct MonsterData*);

int main() {
    struct PlayerData p;
    struct MonsterData m[3];

    initialPlayer(&p);
    for (int i = 0; i < MONSTER_TOTAL; i++)
        initialMonster(&m[i]);

    for (int i = 0; i < MONSTER_TOTAL; i++) {
        printPlayerData(p);
        if (m[i].spd > p.spd) attackM2P(&p, &m[i]);
        while (m[i].hp > 0 && p.hp > 0) {
            attackP2M(&p, &m[i]);
            if (m[i].hp > 0) attackM2P(&p, &m[i]);
        }
        if (m[i].hp > 0) {
            printf("%s死了QQ\n", p.name);
            break;
        } else {
            printf("\\%s被打敗惹/\n"
                "%s獲得經驗值 %d!!\n\n", 
                m[i].type, p.name, m[i].provide_exp);
            p.exp += m[i].provide_exp;
        }
    }

    return 0;
}

void initialPlayer(struct PlayerData *p) {
    printf("請輸入玩家的名字：");
    fgets(p->name, sizeof(p->name), stdin);
    strtok(p->name, "\r\n");
    p->exp = 0;
    p->hp = rand() % 250 + 350;
    p->mp = rand() % 20 + 100;
    p->spd = rand() % 15 + 35;
    p->atk = rand() % 20 + 40;
}

void initialMonster(struct MonsterData *m) {
    strcpy(m->type, "半獸人");
    m->hp = rand() % 20 + 150;
    m->spd = rand() % 15 + 35;
    m->atk = rand() % 10 + 30;
    m->provide_exp = 15;
}

void printPlayerData(struct PlayerData p) {
    printf("%s\n體力 %d\t速度 %d\t攻擊力 %d\t經驗值 %d\n",
        p.name, p.hp, p.mp, p.atk, p.exp);
}

void attackP2M(struct PlayerData *p, struct MonsterData *m) {
    m->hp -= p->atk;
    printf("%s攻擊，造成%s失血 %d 點。\n", p->name, m->type, p->atk);
}

void attackM2P(struct PlayerData *p, struct MonsterData *m) {
    p->hp -= m->atk;
    printf("%s攻擊，造成%s失血 %d 點。\n", m->type, p->name, m->atk);
}