#include <stdio.h>
#include <string.h>

struct borrowerInfo {
    char borrowerID[12], name[20];
    int count;
};

struct bookInfo {
    char bookID[12], title[80], author[80];
    int count;
};

struct dateInfo {
    int year, month, day;
};

struct circulateInfo {
    struct borrowerInfo *borrower;
    struct bookInfo *book;
    struct dateInfo borrowDate, dueDate, returnDate;
};

void printCommands();

int main() {
    struct bookInfo book[50];
    struct borrowerInfo borrower[20];
    struct circulateInfo record[200];
    int bookTotal = 0, borrowerTotal = 0, recordTotal = 0;

    int c;
    printCommands();
    scanf("%d", &c); getchar();

    while (c) {
        if (c == 1) {
            char borrower_id[12], book_id[12];
            struct circulateInfo *r = &record[recordTotal++];

            printf("\nInput the borrower ID: ");
            fgets(borrower_id, sizeof(borrower_id), stdin);
            strtok(borrower_id, "\r\n");
            for (int i = 0; i < borrowerTotal; i++)
                if (!strcmp(borrower_id, borrower[i].borrowerID)) {
                    r->borrower = &borrower[i];
                    break;
                }

            printf("Input the book ID: ");
            fgets(book_id, sizeof(book_id), stdin);
            strtok(book_id, "\r\n");
            for (int i = 0; i < bookTotal; i++)
                if (!strcmp(book_id, book[i].bookID)) {
                    r->book = &book[i];
                    break;
                }

            printf("Input the borrowing data (yy/mm/dd): ");
            scanf("%d/%d/%d", &r->borrowDate.year, &r->borrowDate.month, &r->borrowDate.day);
            printf("Input the due date (yy/mm/dd): ");
            scanf("%d/%d/%d", &r->dueDate.year, &r->dueDate.month, &r->dueDate.day);
        } else if (c == 2) {
            char borrower_id[12], book_id[12];
            struct circulateInfo *r = NULL;
            printf("\nInput the borrower ID: ");
            fgets(borrower_id, sizeof(borrower_id), stdin);
            strtok(borrower_id, "\r\n");

            printf("Input the book ID: ");
            fgets(book_id, sizeof(book_id), stdin);
            strtok(book_id, "\r\n");

            for (int i = 0; i < recordTotal; i++)
                if (!strcmp(borrower_id, record[i].borrower->borrowerID) && !strcmp(book_id, record[i].book->bookID)) {
                    r = &record[i];
                    break;
                }
            if (r == NULL) printf("!!!!!NULL!!!!!\n");
            printf("Input the returning date (yy/mm/dd): ");
            scanf("%d/%d/%d", &r->returnDate.year, &r->returnDate.month, &r->returnDate.day);
            printf("A book has been returned.\n");
        } else if (c == 3) {
            printf("\nData of all borrowers:\n");
            for (int i = 0; i < borrowerTotal; i++)
                printf("Borrower ID: %s, Name: %s\n", borrower[i].borrowerID, borrower[i].name);
        } else if (c == 4) {
            printf("\nData of all books:\n");
            for (int i = 0; i < bookTotal; i++)
                printf("Book ID: %s\n  title: %s\nauthors: %s\n", book[i].bookID, book[i].title, book[i].author);
        } else if (c == 5) {
            printf("\nData of all circulation records:\n");
            for (int i = 0; i < recordTotal; i++)
                printf("%s\t%s\t%d/%d/%d\t%d/%d/%d\t%d/%d/%d\n",
                    record[i].borrower->name, record[i].book->bookID,
                    record[i].borrowDate.year, record[i].borrowDate.month, record[i].borrowDate.day,
                    record[i].dueDate.year, record[i].dueDate.month, record[i].dueDate.day,
                    record[i].returnDate.year, record[i].returnDate.month, record[i].returnDate.day);
        } else if (c == 6) {
            printf("Borrower ID: ");
            fgets(borrower[borrowerTotal].borrowerID, sizeof(borrower[borrowerTotal].borrowerID), stdin);
            strtok(borrower[borrowerTotal].borrowerID, "\r\n");

            printf("Borrower Name: ");
            fgets(borrower[borrowerTotal].name, sizeof(borrower[borrowerTotal].name), stdin);
            strtok(borrower[borrowerTotal].name, "\r\n");

            borrower[borrowerTotal].count = 0;
            borrowerTotal++;
        } else if (c == 7) {
            printf("Book ID: ");
            fgets(book[bookTotal].bookID, sizeof(book[bookTotal].bookID), stdin);
            strtok(book[bookTotal].bookID, "\r\n");

            printf("Book title: ");
            fgets(book[bookTotal].title, sizeof(book[bookTotal].title), stdin);
            strtok(book[bookTotal].title, "\r\n");

            printf("Authors: ");
            fgets(book[bookTotal].author, sizeof(book[bookTotal].author), stdin);
            strtok(book[bookTotal].author, "\r\n");

            book[bookTotal].count = 1;
            bookTotal++;
        }
        printCommands();
        scanf("%d", &c); getchar();
    }
    printf("Thanks for using NTOU CSE Library Curculation System!!\n");
    return 0;
}

void printCommands() {
    printf(
        "\nMenu of NTOU CSE Library Circulation System\n"
        " 1. Borrow a book.\n"
        " 2. Return a book.\n"
        " 3. Print all the borrowers.\n"
        " 4. Print all the books.\n"
        " 5. Print all the circulation records.\n"
        " 6. Add a new borrower.\n"
        " 7. Add a new book.\n"
        " 8. Find top-1 popular books of a year.\n"
        " 9. Find top-1 borrowers of a year.\n"
        // "10. Create a monthly table of circulations of a year.\n"
        // "11. List all borrowers who have not returned overdue books.\n"
        " 0. Exit.\n"
        "Please choose one action: "
    );
}