#include <stdio.h>

int main() {
    int n, i;

    printf("Please input a positive number: ");
    scanf("%d", &n);

    for (i = 1; i <= n; i++)
        printf("%d / %d = %lf\n", i, i+1, i*1.0/(i+1));

    return 0;
}