#include <stdio.h>
#include <string.h>

char *twoCharFMname[] = {"軒轅", "歐陽", "司馬", "司徒", "司空", "司寇", "上官", "尉遲", "羊舌", "第五", "梁丘", "鍾離", "東郭", "公孫", "孟孫", "仲孫", "叔孫", "季孫", "長孫", "慕容", "宇文", "閭丘", "諸葛", "東方", "東門", "西門", "公羊", "夏侯", "万俟", "百里", "端木", "公冶", "皇甫", "呼延", "浮屠", "令狐", "淳于", "即墨", "單于", "南宮", "田丘", "伊能"};
char *oneCharFMname[] = { "王", "李", "張", "劉", "陳", "楊", "黃", "吳", "趙", "周", "徐", "孫", "馬", "朱", "胡", "林", "郭", "何", "高", "羅", "鄭", "梁", "謝", "宋", "唐", "許", "鄧", "馮", "韓", "曹", "曾", "彭", "蕭", "蔡", "潘", "田", "董", "袁", "于", "余", "葉", "蔣", "杜", "蘇", "魏", "程", "呂", "丁", "沈", "任", "姚", "盧", "傅", "鍾", "姜", "崔", "譚", "廖", "范", "汪", "陸", "金", "石", "戴", "賈", "韋", "夏", "邱", "方", "侯", "鄒", "熊", "孟", "秦", "白", "江", "閻", "薛", "尹", "段", "雷", "黎", "史", "龍", "陶", "賀", "顧", "毛", "郝", "龔", "邵", "萬", "錢", "嚴", "賴", "覃", "洪", "武", "莫", "孔", };

int check(char *name) {
    for (int i = 0; i < 42; i++)
        if (!strncmp(name, twoCharFMname[i], 6)) return 2;
    for (int i = 0; i < 100; i++)
        if (!strncmp(name, oneCharFMname[i], 3)) return 1;
    return 0;
}

int main() {
    freopen("in.txt", "r", stdin);
    char s[128];

    scanf("%s", s);

    while (strcmp(s, "quit")) {
        printf("名字：[%s]\n", s);
        int i = check(s);

        int len = strlen(s);
        char first[12];
        char last[12];
        if (len == 12) {
            strncpy(first, s, 6);
            strcpy(last, s + 6);
            first[6] = 0;
        } else if (len == 6) {
            strncpy(first, s, 3);
            strcpy(last, s + 3);
            first[3] = 0;
        } else {
            if (check(s + 3)) {
                strncpy(first, s, 3); first[3] = 0;
                strncpy(last, s, 6); last[6] = 0;
                printf("請問您姓 (1) %s 還是 (2) %s？", first, last);
                int a;
                scanf("%d", &a);
                printf("%d\n", a);
                if (a == 1)
                    strcpy(last, s + 3);
                else
                    strcpy(first, last), strcpy(last, s + 6);
            } else {
                strncpy(first, s, 3);
                strcpy(last, s + 3);
                first[3] = 0;
            }
        }

        printf("歡迎 %s 同學 %s 的蒞臨！\n", first, last);
        scanf("%s", s);
    }

    return 0;
}