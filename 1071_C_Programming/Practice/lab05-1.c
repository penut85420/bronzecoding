#include <stdio.h>
#include <stdlib.h>

int main() {
	int a, b, i, arr[11] = {0}, n = 10000;

	for (i = 0; i < n; i++)
		arr[rand() % 6 + rand() % 6]++;
	
	for (i = 0; i < 11; i++)
		// printf("%2d occurs %5.2lf times\n", i + 2, arr[i] * 100.0 / n);
		printf("%2d occurs %5d times\n", i + 2, arr[i]);

	return 0;
}