#include <stdio.h>

int main() {
	int i, arr[51];
	
	arr[1] = 0; arr[2] = 1;

	for (i = 3; i <= 50; i++)
		arr[i] = arr[i-1] + arr[i-2];

	printf("Which Fibonacci number do you want to see (-1 to exit): ");
	scanf("%d", &i);
	while (i != -1) {
		printf("The %d-th Fibonacci number is %d.\n", i, arr[i]);
		printf("Which Fibonacci number do you want to see (-1 to exit): ");
		scanf("%d", &i);
	}

	return 0;
}