#include <stdio.h>

int main() {
    int N, n, i, j;

    printf("How many sets of test data: ");
    scanf("%d", &N);
    while (N--) {
        printf("\nSize: ");
        scanf("%d", &n);

        for (i = 0; i < n; i++) {
            for (j = 0; j < n; j++) {
                if (i + j > n - 6 - 1 && i + j < n)
                    printf("$");
                else printf("+");
            }
            printf("\n");
        }
    }

    return 0;
}