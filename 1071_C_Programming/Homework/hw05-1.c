#include <stdio.h>

int main() {
    int i, j, k, m, n, p, N;

    printf("A is a matrix with m x n elements and\n");
    printf("B is a matrix with n x p elements.\n");
    printf("\nHow many sets of test data: ");
    scanf("%d", &N);

    while (N--) {
        printf("\nInput values of m, n, p: ");
        scanf("%d %d %d", &m, &n, &p);
        int A[m][n], B[n][p], C[m][p];

        printf("Input numbers in matrix A: ");
        for (i = 0; i < m; i++)
            for (j = 0; j < n; j++)
                scanf("%d", &A[i][j]);

        printf("Input numbers in matrix B: ");
        for (i = 0; i < n; i++)
            for (j = 0; j < p; j++)
                scanf("%d", &B[i][j]);

        for (i = 0; i < m; i++)
            for (j = 0; j < p; j++)
                for (C[i][j] = 0, k = 0; k < n; k++)
                    C[i][j] += A[i][k] * B[k][j];

        printf("Numbers in matrix C are:\n{");
        printf("{%d", C[i = 0][j = 0]);
        for (j = 1; j < p; j++)
            printf(", %d", C[i][j]);
        printf("}");
        
        for (i = 1; i < m; i++) {
            printf(",\n{%d", C[i][j = 0]);
            for (j = 1; j < p; j++)
                printf(", %d", C[i][j]);
            printf("}");
        } printf("}\n");
    }

    return 0;
}