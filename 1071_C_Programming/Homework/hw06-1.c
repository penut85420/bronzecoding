#include <stdio.h>
#include <math.h>

int gcd(int a, int b) {

    while (a && b) {
        a %= b;
        if (!a && b) return b;
        b %= a;
        if (!b && a) return a;
    }
    return 1;
}

int main() {
    const double PI = 3.14159265358979323846;
    int n, i;
    printf("n = ");
    scanf("%d", &n);
    for (i = 0; i <= (n * 2); i++) {
        int d = gcd(i, n);

        printf("x = ");
        if ((i / d) % (n / d))
            printf("%d/%d", i/d, n/d);
        else printf("%d", i/d/(n/d));
        double t = 1.0 * i / d / (n / d) * PI;
        printf(" PI, sin(x) = %.6lf, cos(x) = %.6lf\n", sin(t), cos(t));
    }

    return 0;
}