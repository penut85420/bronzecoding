#include <stdio.h>

int main() {
    int n, i, begin, end, ratio, sum;

    printf("How many sets of test data: ");
    scanf("%d", &n);

    while (n--) {
        printf("\nThe first number: ");
        scanf("%d", &begin);
        printf("The last number: ");
        scanf("%d", &end);
        printf("Common ratio: ");
        scanf("%d", &ratio);

        for (sum = 0, i = begin; i <= end; i *= ratio)
            printf("%d\n", i), sum += i;

        printf("The sum of this geometric series is %d.\n", sum);
    }

    return 0;
}