#include <stdio.h>

int a(int);
int b(int);

int a(int n) {
    if (n == 1) return 1;
    return a(n-1) + 2 * b(n-1) + 1;
}

int b(int n) {
    if (n == 1) return 1;
    return 3 * a(n-1) - b(n-1) + 1;
}

int main() {
    int N, n;
    
    printf("How many sets of test data: ");
    scanf("%d", &N);

    while (N--) {
        printf("\nWhat is the upper limit? ");
        scanf("%d", &n);

        for (int i = 1; a(i) <= n && b(i) <= n; i++)
            printf("n = %d, (a, b) = (%d, %d)\n", i, a(i), b(i));
    }

    return 0;
}