#include <stdio.h>
#include <string.h>

int main() {
    int n, k, len;
    char s[512];
    printf("How many sets of test data: ");
    scanf("%d", &n); while (getchar() != '\n');

    while (n--) {
        printf("\nInput a string: ");
        fgets(s, sizeof(s), stdin);
        strtok(s, "\r\n");
        len = strlen(s);
        printf("Skipping = ");
        scanf("%d", &k); while (getchar() != '\n');

        for (int i = 0; i < len; i += k)
            printf("[%s]\n", s + i);
    }

    return 0;
}