#include <stdio.h>

int main() {
    int n, i, num;
    char *fs[] = {"", "%d", "%o", "%x"};

    printf("How many sets of test data: ");
    scanf("%d", &n);

    while (n--) {
        printf("\nInput in what base (1) decimal (2) octal (3) hexadecimal: ");
        scanf("%d", &i);
        printf("Input a number: ");
        scanf(fs[i], &num);
        printf("This number is\n");
        printf("%d in decimal\n", num);
        printf("%o in octal, and\n", num);
        printf("%x in hexadecimal.\n", num);
    }

    return 0;
}