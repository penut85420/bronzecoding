#include <stdio.h>

int isLeapYear(int yy) {
    if ((yy % 4 == 0 && yy % 100 != 0) || yy % 400 == 0)
        return 1;
    return 0;
}

int isInvalidDate(int yy, int mm, int dd) {
    int days[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    if (isLeapYear(yy)) days[1] = 29;
    if (mm < 1 || mm > 12) return 1;
    if (dd < 1 || dd > days[mm-1]) return 1;

    return 0;
}

int main() {
    int ti, repeatTimes, year, month, day;

    printf("How many sets of test data: ");
    scanf("%d", &repeatTimes);

    for (ti = 0; ti < repeatTimes; ti++) {
        printf("\nInput a date (year/month/day): ");
        scanf("%d/%d/%d", &year, &month, &day);
        printf("%d/%d/%d %s a valid date.\n", year, month, day, isInvalidDate(year, month, day) ? "is not" : "is");
    }
    return 0;
}