#include <stdio.h>

int main() {
    int arr[100], n, i;

    printf("How many numbers: ");
    scanf("%d", &n);
    while (n <= 0) {
        printf("How many numbers: ");
        scanf("%d", &n);
    }

    printf("Please input numbers: ");
    for (i = 0; i < n; i++)
        scanf("%d", &arr[i]);

    printf("\nWhich index do you want to see (-1 to exit): ");
    scanf("%d", &i);
    while (i != -1) {
        printf("a[%d] is %d\n", i, arr[i]);
        printf("\nWhich index do you want to see (-1 to exit): ");
        scanf("%d", &i);
    }

    printf("Over!\n");

    return 0;
}