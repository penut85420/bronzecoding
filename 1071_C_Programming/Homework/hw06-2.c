#include <stdio.h>

int isPrime(int n) {
    if (n == 1) return 0;
    if (n % 2 == 0 && n != 2) return 0;
    for (int i = 3; i < n; i += 2)
        if (n % i == 0) return 0;
    return 1;
}

int main() {
    int n, num, i, j, k;
    printf("How many sets of test data: ");
    scanf("%d", &n);

    while (n--) {
        printf("\nInput a number (>= 6): ");
        scanf("%d", &num);
        for (i = 2; i <= num; i++) {
            if (!isPrime(i)) continue;
            for (j = i; j <= num; j++) {
                if (!isPrime(j)) continue;
                for (k = j; k <= num; k++) {
                    if (!isPrime(k)) continue;
                    if (i + j + k == num)
                        printf("%d = %d + %d + %d\n", num, i, j, k);
                }
            }
        }
    }

    return 0;
}