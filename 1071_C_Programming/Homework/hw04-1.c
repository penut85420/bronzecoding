#include <stdio.h>

int main() {
    int N, n;

    printf("How many sets of test data: ");
    scanf("%d", &N);

    while (N--) {
        printf("\nInput a number: ");
        scanf("%d", &n);
        printf("Reverse: ");
        while (n) {
            printf("%d", n % 10);
            n /= 10;
        }
        printf("\n");
    }

    return 0;
}