#include <stdio.h>

int main() {
    int n, it;
    unsigned int num;

    printf("How many sets of test data: ");
    scanf("%d", &n);

    while (n--) {
        printf("\nPlease input a large integer: ");
        scanf("%u", &num);
        printf("Please input an interval: ");
        scanf("%d", &it);
        printf("Your input is [%u]\n", num);
        printf("Its next 10 numbers by adding %d are:\n", it);
        for (int i = 1; i <= 10; i++)
            printf("%u\n", num + it * i);
        printf("Over!!\n");
    }

    return 0;
}