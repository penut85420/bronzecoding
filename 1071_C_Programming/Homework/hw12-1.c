#include <stdio.h>
#include <string.h>

int main() {
    char s[512];
    char t[512];
    printf("Input a string: ");
    fgets(s, 512, stdin);
    printf("Divided by what symbols: ");
    fgets(t, 512, stdin);

    char *p = strtok(s, t);
    
    while (p != NULL) {
        printf("[%s] length = %d\n", p, strlen(p));
        p = strtok(NULL, t);
    }

    return 0;
}