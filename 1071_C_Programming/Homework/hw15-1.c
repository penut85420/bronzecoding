#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    char name[32];
    int yy, mm, dd, donation;
} donator;

int main() {
    int i, n, m, maxlen = 0;
    donator *d;
    char in[512];

    printf("請輸入捐款筆數：");
    scanf("%d", &n); getchar();

    d = (donator*) malloc (sizeof(donator) * n);

    for (i = 0; i < n; i++) {
        printf("請輸入第 %d 位捐款人姓名、捐款日期 (年/月/日) 和金額：", i + 1);
        fgets(in, sizeof(in), stdin);
        m = sscanf(in, "%s %d/%d/%d %d", d[i].name, &d[i].yy, &d[i].mm, &d[i].dd, &d[i].donation);

        while (m < 5) {
            printf("@@ 輸入格式不正確！只讀到 %d 個變數。\n", m);
            printf("請輸入第 %d 位捐款人姓名、捐款日期 (年/月/日) 和金額：", i + 1);
            fgets(in, sizeof(in), stdin);
            m = sscanf(in, "%s %d/%d/%d %d", d[i].name, &d[i].yy, &d[i].mm, &d[i].dd, &d[i].donation);
        }
        int t = strlen(d[i].name);
        if (t > maxlen) maxlen = t;
    }

    for (i = 0; i < n; i++)
        printf("%04d/%02d/%02d [%-*s] $%7d\n", d[i].yy, d[i].mm, d[i].dd, maxlen, d[i].name, d[i].donation);

    return 0;
}