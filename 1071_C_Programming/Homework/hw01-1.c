#include <stdio.h>

int main() {
    int n, i, d;

    printf("How many sets of test data: ");
    scanf("%d", &n);

    for (i = 0; i < n; i++) {
        printf("\nInput a number: ");
        scanf("%d", &d);

        if (d % 5 == 0) printf("%d is a multiple of 5.\n", d);
        if (d % 7 == 0) printf("%d is a multiple of 7.\n", d);
    }

    printf("\nOver!\n");
    return 0;
}