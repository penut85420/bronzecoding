#include <stdio.h>

typedef struct {
    int yy, mm, dd;
} DateData;

typedef struct {
    int seatNo;
    char name[20];
    DateData birthdate;
} StudentInfo;

void getdate(DateData *d) {
    scanf("%d/%d/%d", &d->yy, &d->mm, &d->dd);
}

int datecmp(DateData date1, DateData date2) {
    if (date1.yy > date2.yy) return  1;
    if (date1.yy < date2.yy) return -1;
    if (date1.mm > date2.mm) return  1;
    if (date1.mm < date2.mm) return -1;
    if (date1.dd > date2.dd) return  1;
    if (date1.dd < date2.dd) return -1;
    return 0;
}

int main() {
    int n;
    StudentInfo si[50];

    printf("How many students? ");
    scanf("%d", &n);

    for (int i = 0; i < n; i++) {
        printf("student[%d].seatNo: ", i);
        scanf("%d", &si[i].seatNo);
        printf("student[%d].name: ", i);
        scanf("%s", si[i].name);
        printf("student[%d].birthdate: ", i);
        getdate(&si[i].birthdate);
    }

    int low, upp;
    printf("Lower range: ");
    scanf("%d", &low);
    printf("Upper range: ");
    scanf("%d", &upp);
    
    StudentInfo st = si[upp];
    for (int i = low; i < upp; i++) {
        if (datecmp(st.birthdate, si[i].birthdate) == -1)
            st = si[i];
    } printf(
        "The youngest student from student[%d] to student[%d] is:\n"
        "%s, No. %d, whose birthdate is %d/%d/%d.\n",
        low, upp, st.name, st.seatNo, st.birthdate.yy, st.birthdate.mm, st.birthdate.dd
    );

    return 0;
}