#include <stdio.h>

int main() {
    long long int n, a, b;
    
    printf("How many sets of test data: ");
    scanf("%lld", &n);

    while (n--) {
        printf("\nNumber: ");
        scanf("%lld", &a);
        printf("Position: ");
        scanf("%lld", &b);

        if (a & (1 << b)) printf("It is a 1 bit.\n");
        else printf("It is a 0 bit.\n");
    }

    return 0;
}