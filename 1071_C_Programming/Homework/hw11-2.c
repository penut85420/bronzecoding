#include <stdio.h>
#include <string.h>

void reverseStr(char str[]) {
	char *p = str, *q = str + strlen(str) - 1;
	while (p < q) {
		char c = *p;
		*p = *q;
		*q = c;
		p++;
		q--;
	}
}

int main() {
    char inputStr[512];
    int ti, repeatTimes;
    printf("How many sets of test data: ");
    scanf("%d", &repeatTimes);
	getchar();
    for (ti = 0; ti < repeatTimes; ti++) {
        printf("\nInput a string: ");
        fgets(inputStr, 512, stdin);
		strtok(inputStr, "\n");
        reverseStr(inputStr);
        printf("The reversed string is [%s]\n", inputStr);
    }
    return 0;
}
