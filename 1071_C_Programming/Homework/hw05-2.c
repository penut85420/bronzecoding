#include <stdio.h>

int isLeapYear(int yy) {
    if ((yy % 4 == 0 && yy % 100 != 0) || yy % 400 == 0)
        return 1;
    return 0;
}

int main() {
    int ti, repeatTimes, year;
    printf("How many sets of test data: ");
    scanf("%d", &repeatTimes);
    for (ti = 0; ti < repeatTimes; ti++) {
        printf("\nInput a year: "); scanf("%d", &year);
        printf("%d %s a leap year.\n", year, isLeapYear(year) ? "is" : "is not");
    }
    return 0;
}