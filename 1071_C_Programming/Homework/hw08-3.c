#include <stdio.h>

int mya(int k, int m, int n, int s, int t) {
    if (!k) return 0;
    if (k % 2) return m * mya(k-1, m, n, s, t) + n;
    return s * mya(k-1, m, n, s, t) + t;
}

int main() {
    int m, n, s, t, i, x;
    
    printf("How many sets of test data: ");
    scanf("%d", &i);

    while (i--) {
        printf("\nm = ");
        scanf("%d", &m);
        printf("n = ");
        scanf("%d", &n);
        printf("s = ");
        scanf("%d", &s);
        printf("t = ");
        scanf("%d", &t);
        printf("Final index = ");
        scanf("%d", &x);
        for (int j = 0; j <= x; j++)
            printf("a_%d = %d\n", j, mya(j, m, n, s, t));
    }

    return 0;
}