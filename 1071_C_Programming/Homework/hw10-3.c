#include <stdio.h>

int isAlpha(char c) {
    if (c >= 'a' && c <= 'z') return 1;
    if (c >= 'A' && c <= 'Z') return 1;
    return 0;
}
void reverseCase(char newStr[], char *orgStr) { 
    int i;
    for (i = 0; orgStr[i]; i++)
        if (isAlpha(orgStr[i])) newStr[i] = (orgStr[i] | 32) ^ 32;
        else newStr[i] = orgStr[i];
    newStr[i - 1] = 0;
}

int main() {
    char newStr[512], orgStr[512];
    int ti, repeatTimes;

    printf("How many sets of test data: ");
    scanf("%d", &repeatTimes); while (getchar() != '\n');

    for (ti = 0; ti < repeatTimes; ti++) {
        printf("\nInput a string: ");
        fgets(orgStr, sizeof(orgStr), stdin);
        reverseCase(newStr, orgStr);
        printf("The new string is [%s]\n", newStr);
    }
    return 0;
}