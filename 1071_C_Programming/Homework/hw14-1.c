#include <stdio.h>

int main() {
    long long int n, a;
    
    printf("How many sets of test data: ");
    scanf("%lld", &n);

    while (n--) {
        printf("\nInput: ");
        scanf("%lld", &a);

        printf("The 1 bits are at:");
        for (long long int i = 1, j = 0; i <= a; i <<= 1, j++) {
            if (i & a) printf(" %lld", j);
            // printf("[%d %d %d]\n", i, a, i & a);
        }
        printf("\n");
    }

    return 0;
}