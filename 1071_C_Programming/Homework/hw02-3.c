#include <stdio.h>

int main() {
    int n, i, price, age, gender;

    printf("How many sets of test data: ");
    scanf("%d", &n);

    for (i = 0; i < n; i++) {
        printf("\nOriginal Price: ");
        scanf("%d", &price);
        printf("Age: ");
        scanf("%d", &age);
        printf("Gender (0=female, 1=male): ");
        scanf("%d", &gender);

        if (age >= 65)
            price = price * 6 / 10;
        else if (!gender)
            price = price * 9 / 10;

        printf("Real price is %d.\n", price);
    }

    return 0;
}