#include <stdio.h>

int main() {
    int N, i, n[9], x[100], y[100];
    char s[9][3] = {
        "x1", "y1", "a", "b", "c", "r", "s", "t", "n"
    };

    printf("How many sets of test data: ");
    scanf("%d", &N);
    while (N--) {
        printf("\n");
        for (i = 0; i < 9; i++) {
            printf("%s = ", s[i]);
            scanf("%d", &n[i]);
        }

        x[0] = n[0];
        y[0] = n[1];
        i = 0;
        printf("n = %d, (x, y) = (%d, %d)\n", i+1, x[i], y[i]);
        for (i = 1; i < n[8]; i++) {
            x[i] = n[2] * x[i-1] + n[3] * y[i-1] + n[4];
            y[i] = n[5] * x[i-1] + n[6] * y[i-1] + n[7];
            printf("n = %d, (x, y) = (%d, %d)\n", i+1, x[i], y[i]);
        }
            
    }

    return 0;
}